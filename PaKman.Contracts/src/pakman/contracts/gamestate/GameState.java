package pakman.contracts.gamestate;

import java.io.Serializable;
import java.util.Set;

import pakman.contracts.characters.Enemy;
import pakman.contracts.characters.Hero;
import pakman.contracts.labyrinth.Field;
import pakman.contracts.labyrinth.Map;

public interface GameState extends Serializable {
	Hero getHero1();

	Hero getHero2();

	Set<Enemy> getEnemies();

	Map getMap();

	void setMap(Map map);

	int getTotalScore();

	void setPlayerScore(int score);

	int getPlayerScore();

	void addReward(int reward);

	Field getMapStateField(int row, int column);

	void resetCharacters();

	boolean isGameOver();

	void setGameOver(boolean gameOver);

	boolean isHeroKilled();

	void setGameStarted(boolean gameStarted);

	boolean isGameStarted();

	void setHeroKilled(boolean killed);

	GameType getGameType();

	void setGameType(GameType gameType);

	boolean isGamePrepared();

	void setGamePrepared(boolean prepared);

	GameState getDeepCopy();
}
