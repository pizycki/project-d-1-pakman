package pakman.contracts.gamestate;

import pakman.contracts.characters.Enemy;
import pakman.contracts.characters.Hero;
import pakman.contracts.labyrinth.Map;

public interface GameStateFactory {
	GameState createNewSession(Hero hero1, Hero hero2, Enemy e1,
			Enemy e2, Enemy e3, Enemy e4, Map map);
}
