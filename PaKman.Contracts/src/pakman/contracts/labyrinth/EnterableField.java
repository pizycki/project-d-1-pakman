package pakman.contracts.labyrinth;

import pakman.common.Direction;

public interface EnterableField extends Field {

	boolean canGoUp();

	boolean canGoRight();

	boolean canGoDown();

	boolean canGoLeft();

	Direction[] getAvaibleDirections();

	void removeDirection(Direction direction);

}
