package pakman.contracts.labyrinth;

import pakman.contracts.consumables.ConsumableItem;

public interface ContainingField extends Field {
	boolean isContainingItem();

	void removeItem();

	ConsumableItem getItem();
}
