package pakman.contracts.labyrinth;

import java.awt.Point;
import java.io.Serializable;

public interface Field extends Serializable {

	Point getLocation();

	Point getCenterPoint();

	int getLocX();

	int getLocY();

	int getWidth();

	int getHeight();

}
