package pakman.contracts.labyrinth;

import java.io.Serializable;

public interface Map extends Serializable {
	Field[][] getMapState();

	int getFieldsRow(int y);

	int getFieldsColumn(int x);

	Field getField(int x, int y);
}
