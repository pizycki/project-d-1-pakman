package pakman.contracts.labyrinth;

public interface MapFactory {
	Map create();
}
