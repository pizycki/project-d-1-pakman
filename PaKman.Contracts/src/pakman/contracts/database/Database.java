package pakman.contracts.database;

import java.io.FileNotFoundException;
import java.util.List;

public interface Database {
	void addNewRecord(String nick, int score);

	void clearRecordList();

	void createRecordList() throws FileNotFoundException;

	void saveRecordList() throws FileNotFoundException;

	RecordI getTopRecord();

	void makeRecord(int score);
	
	List<RecordI> getList();
}
