package pakman.contracts.database;

public interface RecordI {
	String getNick();
	int getScore();
	String[] toTable();
}
