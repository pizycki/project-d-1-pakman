package pakman.contracts.sound;

public interface Sound {
	String getName();

	String getFilePath();

	long getLength();

}
