package pakman.contracts.sound;

import pakman.contracts.sound.Sound;

public interface SoundController {
	void playSound(final Sound sound);
	void stopSound();
}
