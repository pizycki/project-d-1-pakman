package pakman.contracts.sound;

public interface SoundFactory {
	Sound create(String name, String filePath, long length);
}
