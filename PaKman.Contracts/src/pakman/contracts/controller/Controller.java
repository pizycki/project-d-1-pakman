package pakman.contracts.controller;

public interface Controller {
	void addControllerListener(ControllerListener listener);
	void removeControllerListener(ControllerListener listener);
}