package pakman.contracts.gui;

import pakman.contracts.gamestate.GameState;

public interface Gui {

	void show();

	void updateGame();

	void prepareNewGame(GameState gameState);

	void addNewGameSelectedEventListener(NewGameSelectedEventListener listener);

	void removeNewGameSelectedEventListener(
			NewGameSelectedEventListener listener);

	void addPlayerDirectionSelectEventListener(
			PlayerDirectionSelectEventListener listener);

	void removePlayerDirectionSelectEventListener(
			PlayerDirectionSelectEventListener listener);

	void showMenuWindow();
}
