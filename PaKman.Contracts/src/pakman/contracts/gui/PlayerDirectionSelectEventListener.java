package pakman.contracts.gui;

import pakman.common.Direction;

public interface PlayerDirectionSelectEventListener {
	void onPlayerDirectionSelectEvent(Direction direction);
}
