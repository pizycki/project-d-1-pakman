package pakman.contracts.gui;

public interface NewGameSelectedEventListener {
	void onNewGameSelectedEvent();
}
