package pakman.contracts.gui;

@SuppressWarnings("serial")
public class GuiException extends Exception {
	public GuiException(String msg) {
		super(msg);
	}
}
