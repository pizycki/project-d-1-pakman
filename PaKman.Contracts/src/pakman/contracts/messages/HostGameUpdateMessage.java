package pakman.contracts.messages;

import pakman.common.Direction;
import pakman.contracts.gamestate.GameType;

public interface HostGameUpdateMessage extends Message {
	int getH1X();

	void setH1X(int h1x);

	int getH1Y();

	void setH1Y(int h1y);

	Direction getH1dir();

	void setH1dir(Direction h1dir);

	int getE1X();

	void setE1X(int e1x);

	int getE1Y();

	void setE1Y(int e1y);

	Direction getE1dir();

	void setE1dir(Direction e1dir);

	int getE2X();

	void setE2X(int e2x);

	int getE2Y();

	void setE2Y(int e2y);

	Direction getE2dir();

	void setE2dir(Direction e2dir);

	int getE3X();

	void setE3X(int e3x);

	int getE3Y();

	void setE3Y(int e3y);

	Direction getE3dir();

	void setE3dir(Direction e3dir);

	int getE4X();

	void setE4X(int e4x);

	int getE4Y();

	void setE4Y(int e4y);

	Direction getE4dir();

	void setE4dir(Direction e4dir);

	boolean isConsumedItem();

	void setConsumedItem(boolean consumedItem);

	int getItemX();

	void setItemX(int itemX);

	int getItemY();

	void setItemY(int itemY);

	int getPlayerScore();

	void setPlayerScore(int playerScore);

	int getLivesLeft();

	void setLivesLeft(int livesLeft);

	boolean isHeroKilled();

	void setHeroKilled(boolean heroKilled);

	boolean isGameOver();

	void setGameOver(boolean gameOver);

	boolean isGameStarted();

	void setGameStarted(boolean gameStarted);

	boolean isGamePrepared();

	void setGamePrepared(boolean gamePrepared);

	GameType getGameType();

	void setGameType(GameType gameType);
}
