package pakman.contracts.messages;

import pakman.contracts.gamestate.GameType;

public interface ClientCreateNewSession extends Message {
	GameType getGameType();

	void setGameType(GameType gameType);

	int getPlayerOnePort();

	void setPlayerOnePort(int playerOnePort);

	int getPlayerTwoPort();

	void setPlayerTwoPort(int playerTwoPort);
}
