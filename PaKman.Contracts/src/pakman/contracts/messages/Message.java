package pakman.contracts.messages;

public interface Message {
	MessageType getMessageType();
}
