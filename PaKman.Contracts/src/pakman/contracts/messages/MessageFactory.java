package pakman.contracts.messages;

public interface MessageFactory {
	Message create(MessageType msgType);
}
