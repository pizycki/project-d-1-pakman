package pakman.contracts.messages;

import pakman.common.Direction;

public interface ClientGameUpdateMessage extends Message {
	Direction getPlayerDirection();

	void setPlayerDirection(Direction playerDirection);
}
