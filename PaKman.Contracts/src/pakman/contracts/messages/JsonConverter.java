package pakman.contracts.messages;

public interface JsonConverter {
	Class<?> getMessageType(String json);

	<T> String convertTo(T msg);

	boolean isValid(String json);

	<T> T convertFrom(Class<T> clazz, String rawJson);

	String fixJson(String brokenJson);
}
