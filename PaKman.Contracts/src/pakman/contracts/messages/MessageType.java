package pakman.contracts.messages;

public enum MessageType {
	Undifined, ClientGameUpdate, HostGameUpdate, ClientCreateNewSession, HostWaitingForPlayers, HostSessionCreated
}
