package pakman.contracts.messages;

import pakman.contracts.gamestate.GameType;

public interface HostSessionCreated extends Message {
	void setGameType(GameType gameType);

	GameType getGameType();
}
