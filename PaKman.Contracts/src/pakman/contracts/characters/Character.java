package pakman.contracts.characters;

import java.awt.Point;
import java.io.Serializable;

import pakman.common.Direction;

public interface Character extends Serializable {

	void makeStep();

	float getLocX();

	void setLocX(float x);

	float getLocY();

	void setLocY(float y);

	String getName();

	void setName(String name);

	Direction getDirection();

	void setDirection(Direction direction);

	void setStepLength(float speed);

	float getStepLength();

	Point getCenterPoint();

	int getWidth();

	int getHeight();

}
