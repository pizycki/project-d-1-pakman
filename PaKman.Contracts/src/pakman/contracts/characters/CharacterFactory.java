package pakman.contracts.characters;

import pakman.common.Direction;

public interface CharacterFactory {
	// public enum CharacterTypes {
	// Hero, Enemy
	// };

	Hero createHero(String name, int lives, Direction direction,
			int stepLength, int locX, int locY);

	Enemy createEnemy(String name, Direction direction, int stepLength,
			int locX, int locY);
}
