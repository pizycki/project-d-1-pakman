package pakman.contracts.consumables;

import java.io.Serializable;

public interface ConsumableItem extends Serializable {
	int getReward();
}
