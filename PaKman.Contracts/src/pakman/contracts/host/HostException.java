package pakman.contracts.host;

@SuppressWarnings("serial")
public class HostException extends Exception {
	public HostException(String msg) {
		super(msg);
	}
}
