package pakman.contracts.host;

public interface GameOverEventListener {
	void onGameOverEvent();
}
