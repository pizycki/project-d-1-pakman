package pakman.contracts.host;

import pakman.common.Direction;
import pakman.contracts.gamestate.GameState;
import pakman.contracts.gamestate.GameType;

public interface Host {
	GameState getGameState();

	void setPlayerDirection(Direction direction);

	boolean createNewSession(GameType gameType);

}
