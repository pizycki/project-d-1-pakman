package pakman.contracts.host;

import pakman.contracts.consumables.ConsumableItem;
import pakman.contracts.labyrinth.Field;

public interface ItemConsumedEvent {
	Field getField();

	ConsumableItem getItem();
}
