package pakman.contracts.host;

public interface ItemConsumedEventListener {
	void OnItemConsumed(ItemConsumedEvent event);
}
