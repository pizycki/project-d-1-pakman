package pakman.contracts.host;

public interface NewSessionCreatedListener {
	void onNewSessionCreated();
}
