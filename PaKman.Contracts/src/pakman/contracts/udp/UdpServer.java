package pakman.contracts.udp;

import java.net.DatagramSocket;

public interface UdpServer extends Runnable {

	DatagramSocket getSocket();

	int getTimout();

	void addMessageReceivedEventListener(MessageReceivedEventListener listener);

	void removeMessageReceivedEventListener(
			MessageReceivedEventListener listener);

}
