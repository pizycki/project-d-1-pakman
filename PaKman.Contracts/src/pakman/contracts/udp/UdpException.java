package pakman.contracts.udp;

@SuppressWarnings("serial")
public class UdpException extends Exception {
	public UdpException(String msg) {
		super(msg);
	}
}
