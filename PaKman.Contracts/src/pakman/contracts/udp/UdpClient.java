package pakman.contracts.udp;

import java.io.IOException;

public interface UdpClient {
	void send(byte[] bytes) throws UdpException, IOException;
	void send(String msg) throws UdpException, IOException;
	String sendAndReceive(String msg) throws UdpException, IOException;
}
