package pakman.contracts.udp;

public interface MessageReceivedEvent {
	String getReceivedMsg();
}
