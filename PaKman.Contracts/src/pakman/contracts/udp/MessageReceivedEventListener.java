package pakman.contracts.udp;

public interface MessageReceivedEventListener {

	String onMessageReceivedEvent(MessageReceivedEvent event);
	
}
