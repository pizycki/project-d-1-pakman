package pakman.contracts.udp;

public interface UdpFactory {
	UdpServer createServer(int port, int timeout);

	UdpClient createClient(int port, String host);
}
