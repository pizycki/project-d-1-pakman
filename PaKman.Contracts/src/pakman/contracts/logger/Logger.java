package pakman.contracts.logger;

public interface Logger {
	void WriteLine(String log);
	void WriteLine(String log, String invoker);
	void writeLine(Exception e);
}
