package pakman.contracts.client;

import pakman.contracts.gamestate.GameState;
import pakman.contracts.gui.NewGameSelectedEventListener;
import pakman.contracts.gui.PlayerDirectionSelectEventListener;

public interface Client extends NewGameSelectedEventListener,
		PlayerDirectionSelectEventListener {
	GameState getGameState();

	void createNewRound() throws ClientException;

	String getTitle();
}
