package pakman.contracts.client;

@SuppressWarnings("serial")
public class MapException extends Exception {
	public MapException(String msg) {
		super(msg);
	}
}
