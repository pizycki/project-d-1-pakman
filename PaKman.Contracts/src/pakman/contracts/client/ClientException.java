package pakman.contracts.client;

@SuppressWarnings("serial")
public class ClientException extends Exception {
	public ClientException(String msg) {
		super(msg);
	}
}
