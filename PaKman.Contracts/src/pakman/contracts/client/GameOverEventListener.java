package pakman.contracts.client;

public interface GameOverEventListener {
	void onGameOverEvent();
}
