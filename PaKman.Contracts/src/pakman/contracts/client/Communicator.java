package pakman.contracts.client;

import pakman.contracts.gamestate.GameState;
import pakman.contracts.host.Host;

public interface Communicator extends Host {
	void syncWithHost();

	void setGameState(GameState gamestate);
}
