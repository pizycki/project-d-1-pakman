package pakman.contracts.client;

public interface NewSessionCreatedListener {
	void onNewSessionCreated();
}
