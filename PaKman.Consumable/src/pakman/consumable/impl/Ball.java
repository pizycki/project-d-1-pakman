package pakman.consumable.impl;

import pakman.contracts.consumables.ConsumableItem;

@SuppressWarnings("serial")
public class Ball implements ConsumableItem {

	// ========== <Getters / Setters> ==========
	public int getReward() {
		return this.reward;
	}

	// ========== </Getters / Setters> ==========

	// ========== <Constructors> ==========
	public Ball(int reward) {
		this.reward = reward;
	}

	// ========== </Constructors> ==========

	// ========== <Members> ==========
	int reward;
	// ========== </Members> ==========

	// ========== <Contants> ==========
	public static final int DEFAULT_REWARD_POINTS = 10;
	// ========== </Contants> ==========

}
