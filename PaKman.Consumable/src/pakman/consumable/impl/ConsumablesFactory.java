package pakman.consumable.impl;

import pakman.contracts.consumables.ConsumableItem;
import pakman.contracts.consumables.ConsumbleFactory;

public class ConsumablesFactory implements ConsumbleFactory {

	@Override
	public ConsumableItem createBall(int reward) {
		return new Ball(reward);
	}

}
