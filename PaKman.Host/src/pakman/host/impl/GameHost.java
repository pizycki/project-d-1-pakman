package pakman.host.impl;

import java.util.Timer;
import java.util.TimerTask;

import pakman.common.Direction;
import pakman.contracts.characters.CharacterFactory;
import pakman.contracts.characters.Enemy;
import pakman.contracts.characters.Hero;
import pakman.contracts.gamestate.GameState;
import pakman.contracts.gamestate.GameStateFactory;
import pakman.contracts.gamestate.GameType;
import pakman.contracts.host.GameOverEventListener;
import pakman.contracts.host.Host;
import pakman.contracts.host.HostException;
import pakman.contracts.labyrinth.Map;
import pakman.contracts.labyrinth.MapFactory;
import pakman.contracts.logger.Logger;
import pakman.contracts.messages.JsonConverter;
import pakman.contracts.messages.MessageFactory;
import pakman.contracts.udp.UdpServer;

public class GameHost implements Host, GameOverEventListener {

	// ========== <Events> ==========

	@Override
	public void onGameOverEvent() {
		stopGameUpdateTimer();
		this.controller.getGameState().setGameOver(true);
	}

	private void onTimerTick() {
		controller.moveCharacters();
	}

	// ========== </Events> ==========

	// ========== <Methods> ==========

	/**
	 * 
	 */
	private void startCountingDownToGameStart() {
		Timer countDownTimer = new Timer();
		countDownTimer.schedule(new TimerTask() {
			@Override
			public void run() {
				controller.getGameState().setGameStarted(true);
				startGameUpdateTimer();
			}
		}, 4500); // 4,5 seconds
	}

	/**
	 * Creates game updating timer.
	 */
	protected void startGameUpdateTimer() {
		stopGameUpdateTimer();

		mainTimer = new Timer();
		mainTimer.scheduleAtFixedRate(new TimerTask() {
			@Override
			public void run() {
				onTimerTick();
			}
		}, 0, mainTimerInterval);
	}

	/**
	 * Disposes game updating timer.
	 */
	protected void stopGameUpdateTimer() {
		logger.WriteLine("Stopping updating game.", GameHost.class.getName());
		if (mainTimer != null) {
			mainTimer.cancel();
			mainTimer = null;
		}
	}

	/**
	 * Creates new session.
	 */
	@Override
	public boolean createNewSession(GameType gameType) {
		logger.WriteLine("Creating new session.", GameHost.class.getName());
		try {

			// Map
			Map labyrinth = mapFactory.create();

			// Characters factory
			int stepLength = 1;

			// Create hero 1
			Hero hero1 = this.charsFactory.createHero("Paku", 1,
					Direction.Left, stepLength, 225, 350);
			logger.WriteLine(hero1.toString(), this.toString());

			// Create hero 2
			Hero hero2 = this.charsFactory.createHero("Quaqu", 1,
					Direction.Left, stepLength, 225, 350);
			logger.WriteLine(hero2.toString(), this.toString());

			// Create four enemies
			Enemy e1 = this.charsFactory.createEnemy("Pikej", Direction.Right,
					stepLength, 200, 200);
			logger.WriteLine(e1.toString(), this.toString());
			Enemy e2 = this.charsFactory.createEnemy("Homer", Direction.Up,
					stepLength, 225, 200);
			logger.WriteLine(e2.toString(), this.toString());
			Enemy e3 = this.charsFactory.createEnemy("Rasta", Direction.Up,
					stepLength, 225, 200);
			logger.WriteLine(e3.toString(), this.toString());
			Enemy e4 = this.charsFactory.createEnemy("Hannibal",
					Direction.Left, stepLength, 250, 200);

			// Game state
			GameState gameState = gameStateFactory.createNewSession(hero1,
					hero2, e1, e2, e3, e4, labyrinth);

			// Game controller
			this.controller = new GameController(gameState, logger);

			// Subscribe UDP module for listening removed items
			this.controller.addItemConsumedEventListener(communicator);

			// Subscribe for listening game over event
			this.controller.addGameOverEventListener(this);

			// Set game type.
			controller.getGameState().setGameType(gameType);

			// Start count down to game start.
			startCountingDownToGameStart();

			// Set the game prepared.
			getGameState().setGamePrepared(true);
			logger.WriteLine("Game set to \"prepared\".",
					GameHost.class.getName());

			// New session successfully created, return true.
			return true;
		} catch (Exception e) {
			logger.writeLine(e);
			return false;
		}
	}

	// ========== </Methods> ==========

	// ========== <Getters / Setters> ==========

	@Override
	public GameState getGameState() {
		return this.controller.getGameState();
	}

	@Override
	public void setPlayerDirection(Direction direction) {
		this.controller.setPlayersDirection(direction);

	}

	// ========== </Getters / Setters> ==========

	// ========== <Constructors> ==========
	/**
	 * Constructor
	 * 
	 * @param gameController
	 * @param labyrinthFactory
	 * @param charsFactory
	 * @param gameStateFactory
	 * @throws HostException
	 */
	public GameHost(MapFactory labyrinthFactory,
			CharacterFactory charactersFactory,
			GameStateFactory gameStateFactory, UdpServer udpSrv, Logger logger,
			MessageFactory msgFac, JsonConverter jsonConv) throws HostException {

		// Logger
		if (logger == null)
			throw new HostException("Logger is null!");
		this.logger = logger;
		logger.WriteLine("Initilising...", GameHost.class.getName());

		// Proxy
		if (udpSrv == null)
			throw new HostException("UDP server is null!");
		this.communicator = new HostCommunicator(udpSrv, this, logger, jsonConv, msgFac);

		// Factories
		this.gameStateFactory = gameStateFactory;
		this.mapFactory = labyrinthFactory;
		this.charsFactory = charactersFactory;

		logger.WriteLine("Initialised.", GameHost.class.getName());
	}

	// ========== </Constructors> ==========

	// ========== <Members> ==========
	private GameStateFactory gameStateFactory;
	private MapFactory mapFactory;
	private CharacterFactory charsFactory;
	private GameController controller;
	private Logger logger;
	private HostCommunicator communicator;

	private Timer mainTimer;

	// ========== </Members> ==========

	// ========== <Contants> ==========
	private static final long mainTimerInterval = 10;
	// ========== </Contants> ==========

}
