package pakman.host.impl;

import pakman.contracts.characters.Enemy;
import pakman.contracts.gamestate.GameType;
import pakman.contracts.host.Host;
import pakman.contracts.host.HostException;
import pakman.contracts.host.ItemConsumedEvent;
import pakman.contracts.host.ItemConsumedEventListener;
import pakman.contracts.labyrinth.Field;
import pakman.contracts.logger.Logger;
import pakman.contracts.messages.ClientCreateNewSession;
import pakman.contracts.messages.ClientGameUpdateMessage;
import pakman.contracts.messages.HostGameUpdateMessage;
import pakman.contracts.messages.HostSessionCreated;
import pakman.contracts.messages.JsonConverter;
import pakman.contracts.messages.Message;
import pakman.contracts.messages.MessageFactory;
import pakman.contracts.messages.MessageType;
import pakman.contracts.udp.MessageReceivedEvent;
import pakman.contracts.udp.MessageReceivedEventListener;
import pakman.contracts.udp.UdpServer;

public class HostCommunicator implements MessageReceivedEventListener,
		ItemConsumedEventListener {

	// ========== <Events> ==========

	@Override
	public String onMessageReceivedEvent(MessageReceivedEvent event) {

		if (event == null) {
			logger.WriteLine("Invalid MessageReceivedEvent, message ignored",
					HostCommunicator.class.getName());
			return INVALID_REQUEST;
		}

		String rawMessage = event.getReceivedMsg();
		logger.WriteLine("Raw message content: " + rawMessage,
				HostCommunicator.class.getName());

		// Get type of the message
		Class<?> clazz = jsonConverter.getMessageType(rawMessage);
		Message message = (Message) jsonConverter
				.convertFrom(clazz, rawMessage);

		// When Client Game Update Message received
		if (message instanceof ClientGameUpdateMessage) {
			// Parse received message
			ClientGameUpdateMessage parsedMessage = (ClientGameUpdateMessage) message;

			// Handle message
			handleMessage(parsedMessage);

			// Prepare response for client
			HostGameUpdateMessage hostMsg = prepareMessageForClient();
			// System.out.println("HostMessage przed wys�aniem: "
			// + hostMsg.toString());

			String response = jsonConverter.convertTo(hostMsg);
			logger.WriteLine("Raw response for client content: " + response,
					HostCommunicator.class.getName());

			return response;
		}
		if (message instanceof ClientCreateNewSession) {
			logger.WriteLine("Received message type: "
					+ ClientCreateNewSession.class.getName(),
					HostCommunicator.class.getName());

			// Parse message
			ClientCreateNewSession parsedMessage = (ClientCreateNewSession) message;
			// For Singleplayer
			if (parsedMessage.getGameType() == GameType.Singleplayer) {
				boolean created = host.createNewSession(parsedMessage
						.getGameType());

				if (created) {
					HostSessionCreated hsc = (HostSessionCreated) msgFact
							.create(MessageType.HostSessionCreated);
					hsc.setGameType(parsedMessage.getGameType());

					System.out.println("HostSessionCreated przed wys�aniem: "
							+ hsc.toString());
					String response = jsonConverter.convertTo(hsc);
					logger.WriteLine("Raw response for client content: "
							+ response, HostCommunicator.class.getName());
					return response;
				} else
					return null;

			} else if (parsedMessage.getGameType() == GameType.Multiplayer) {
				// TODO fill
			}
		}

		return new String();
	}

	@Override
	public void OnItemConsumed(ItemConsumedEvent event) {
		hostMessage.setConsumedItem(true);
		hostMessage.setItemX(event.getField().getLocX());
		hostMessage.setItemY(event.getField().getLocY());
	}

	// ========== </Events> ==========

	// ========== <Methods> ==========

	/**
	 * Prepares message for client containing current game state informations.
	 * 
	 * @return
	 */
	protected HostGameUpdateMessage prepareMessageForClient() {
		HostGameUpdateMessage message = getHostMessage();

		// Game started
		message.setGameStarted(this.host.getGameState().isGameStarted());

		// Hero
		message.setH1X((int) this.host.getGameState().getHero1().getLocX());
		message.setH1Y((int) this.host.getGameState().getHero1().getLocY());
		message.setH1dir(this.host.getGameState().getHero1().getDirection());

		// Enemies locations
		Enemy[] enemies = this.host
				.getGameState()
				.getEnemies()
				.toArray(
						new Enemy[this.host.getGameState().getEnemies().size()]);

		// Enemy 1
		message.setE1X((int) enemies[0].getLocX());
		message.setE1Y((int) enemies[0].getLocY());
		message.setE1dir(enemies[0].getDirection());

		// Enemy 2
		message.setE2X((int) enemies[1].getLocX());
		message.setE2Y((int) enemies[1].getLocY());
		message.setE2dir(enemies[1].getDirection());
		// Enemy 3
		message.setE3X((int) enemies[2].getLocX());
		message.setE3Y((int) enemies[2].getLocY());
		message.setE3dir(enemies[2].getDirection());

		// Enemy 4
		message.setE4X((int) enemies[3].getLocX());
		message.setE4Y((int) enemies[3].getLocY());
		message.setE4dir(enemies[3].getDirection());

		/* Consumed items are updated "on live". */

		// Remaining lives
		message.setLivesLeft(this.host.getGameState().getHero1().getLives());

		// Player score
		message.setPlayerScore(this.host.getGameState().getPlayerScore());

		// Hero killed
		message.setHeroKilled(this.host.getGameState().isHeroKilled());

		// Game over
		message.setGameOver(this.host.getGameState().isGameOver());

		// Game type
		message.setGameType(this.host.getGameState().getGameType());

		// Game prepare
		message.setGamePrepared(this.host.getGameState().isGamePrepared());

		return message;

	}

	/**
	 * Handles received game update message type from the client.
	 * 
	 * @param message
	 */
	protected void handleMessage(ClientGameUpdateMessage message) {
		host.setPlayerDirection(message.getPlayerDirection());
	}

	public void setConsumedItem(Field field) {
		int row = field.getLocX();
		int col = field.getLocY();

		// Update host message
		hostMessage.setConsumedItem(true);
		hostMessage.setItemX(col);
		hostMessage.setItemY(row);

	}

	// ========== </Methods> ==========

	// ========== <Getters / Setters> ==========

	/**
	 * 
	 * @return
	 */
	public HostGameUpdateMessage getHostMessage() {
		if (this.hostMessage == null)
			this.hostMessage = (HostGameUpdateMessage) this.msgFact
					.create(MessageType.HostGameUpdate);

		return this.hostMessage;
	}

	/**
	 * 
	 * @return
	 */
	public final UdpServer getUdpSrv() {
		return this.srv;
	}

	// ========== </Getters / Setters> ==========

	// ========== <Constructors> ==========
	public HostCommunicator(UdpServer srv, Host host, Logger logger,
			JsonConverter jsonConverter, MessageFactory msgFac)
			throws HostException {
		this.logger = logger;
		logger.WriteLine("Initilising...", HostCommunicator.class.getName());

		// Assign and run UDP server in parallel thread
		if (srv == null)
			throw new HostException("UdpSrv is null!");
		this.srv = srv;
		new Thread(srv).start();

		// Assign Host
		if (host == null)
			throw new HostException("Host is null!");
		this.host = host;

		// Assign Json Converter
		if (jsonConverter == null)
			throw new HostException("JsonConverter is null!");
		this.jsonConverter = jsonConverter;

		// Assign Message Factory
		if (msgFac == null)
			throw new HostException("MessageFactory is null!");
		this.msgFact = msgFac;

		// Subscribe to event
		this.srv.addMessageReceivedEventListener(this);

		// Prepare host message for client
		this.hostMessage = (HostGameUpdateMessage) this.msgFact
				.create(MessageType.HostGameUpdate);

		logger.WriteLine("Initilised", HostCommunicator.class.getName());

	}

	// ========== </Constructors> ==========

	// ========== <Members> ==========
	private Logger logger;
	private UdpServer srv;
	private Host host;
	private JsonConverter jsonConverter;
	private MessageFactory msgFact;
	protected HostGameUpdateMessage hostMessage;
	// ========== </Members> ==========

	// ========== <Contants> ==========

	public static final String INVALID_REQUEST = "INVALID REQUEST";

	// ========== </Contants> ==========

}
