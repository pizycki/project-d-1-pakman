package pakman.host.impl;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

import pakman.common.Direction;
import pakman.common.DirectionExt;
import pakman.contracts.characters.Character;
import pakman.contracts.characters.Enemy;
import pakman.contracts.characters.Hero;
import pakman.contracts.consumables.ConsumableItem;
import pakman.contracts.gamestate.GameState;
import pakman.contracts.host.GameOverEventListener;
import pakman.contracts.host.ItemConsumedEvent;
import pakman.contracts.host.ItemConsumedEventListener;
import pakman.contracts.labyrinth.ContainingField;
import pakman.contracts.labyrinth.EnterableField;
import pakman.contracts.labyrinth.Field;
import pakman.contracts.labyrinth.Map;
import pakman.contracts.logger.Logger;

/**
 * Controls the game flow.
 * 
 * @author Pawe�
 * 
 */
public class GameController {

	// ========== <Events> ==========

	/**
	 * Raised whenever consumable item is consumed.
	 */
	// ========== <Events - ItemConsumedEvent> ==========
	private Set<ItemConsumedEventListener> itemConsumedEventListenersSet = new HashSet<ItemConsumedEventListener>();

	protected void raiseItemConsumedEvent(Field field, ConsumableItem item) {
		if (this.itemConsumedEventListenersSet != null
				&& this.itemConsumedEventListenersSet.size() != 0) {
			logger.WriteLine("Rising ItemConsumedEvent.",
					GameController.class.getName());
			ItemConsumedEvent event = new ItemConsumedEventImpl(field, item);
			for (ItemConsumedEventListener listener : this.itemConsumedEventListenersSet) {
				listener.OnItemConsumed(event);
			}
		}
	}

	public void addItemConsumedEventListener(ItemConsumedEventListener listener) {
		if (this.itemConsumedEventListenersSet != null && listener != null) {
			this.itemConsumedEventListenersSet.add(listener);
		}
	}

	public void removeItemConsumedEventListener(
			ItemConsumedEventListener listener) {
		if (this.itemConsumedEventListenersSet != null
				&& this.itemConsumedEventListenersSet.size() != 0) {
			this.itemConsumedEventListenersSet.remove(listener);
		}
	}

	// ========== </Events - ItemConsumedEvent> ==========

	/**
	 * Raised when game is finished.
	 */
	// ========== <Events - GameOverEvent> ==========
	private Set<GameOverEventListener> gameOverEventListenersSet = new HashSet<GameOverEventListener>();

	protected void raiseGameOverEvent() {
		if (this.gameOverEventListenersSet != null
				&& this.gameOverEventListenersSet.size() != 0) {
			logger.WriteLine("Rising GameOverEvent.",
					GameController.class.getName());
			for (GameOverEventListener listener : this.gameOverEventListenersSet) {
				listener.onGameOverEvent();
			}
		}
	}

	public void addGameOverEventListener(GameOverEventListener listener) {
		if (this.gameOverEventListenersSet != null && listener != null) {
			this.gameOverEventListenersSet.add(listener);
		}
	}

	public void removeGameOverEventListener(GameOverEventListener listener) {
		if (this.gameOverEventListenersSet != null) {
			this.gameOverEventListenersSet.remove(listener);
		}
	}

	// ========== </Events - GameOverEvent> ==========

	// ========== </Events> ==========

	// ========== <Methods> ==========

	public void moveCharacters() {
		// Move character
		moveHero(this.hero);

		for (Enemy enemy : this.gameState.getEnemies())
			moveEnemy(enemy);

		// Check all enemies and hero position
		boolean touching = false;
		for (Enemy enemy : this.gameState.getEnemies())
			if (checkPositions(this.hero, enemy))
				touching = true;

		// Check if the hero is touching with any enemy.
		if (touching) {
			logger.WriteLine("Hero and an enemy is touching!");

			gameState.getHero1().die();
			if (gameState.getHero1().getLives() == 0) {
				// Raise game over event :(
				raiseGameOverEvent();
			} else {
				gameState.resetCharacters();
			}
		}
	}

	/**
	 * Moves character on the map.
	 * 
	 * @param character
	 */
	private synchronized void moveCharacter(Character character) { // TODO add
																	// enemies
		// specific tactics
		// usage

		EnterableField currentField = (EnterableField) this.labirynth.getField(
				(int) character.getLocX(), (int) character.getLocY());

		// Check is character about to enter the portal (in tunnel)
		if (currentField.getLocY() == 200
				&& (currentField.getLocX() == 0 || currentField.getLocX() == 450)) {

			// Left tunnel
			if (character.getDirection() == Direction.Left) {
				character.setLocX(425);
			}
			// Right tunnel
			else if (character.getDirection() == Direction.Right) {
				character.setLocX(25);
			}
			return;
		}

		// Check is the character in the correct location to change
		// direction.
		// It supposed to be in the center of the cross field.

		boolean allowed = true;

		if ((int) character.getLocX() == currentField.getLocX()
				&& (int) character.getLocY() == currentField.getLocY()) {

			allowed = false;

			if (character instanceof Enemy) {
				Set<Direction> avDirections = new HashSet<Direction>();
				for (Direction direction : currentField.getAvaibleDirections())
					avDirections.add(direction);

				Direction forbiddenDirection = DirectionExt
						.getOpposite(character.getDirection());
				avDirections.remove(forbiddenDirection);
				Direction[] avaDirArr = avDirections
						.toArray(new Direction[avDirections.size()]);

				if (avaDirArr.length == 1) {
					character.setDirection(avaDirArr[0]);
				} else {
					try {
						Random rnd = new Random();
						Direction drawed = avaDirArr[rnd
								.nextInt(avaDirArr.length)];
						character.setDirection(drawed);

					} catch (Exception e) {
						e.printStackTrace();
						System.out.println(character.toString());
					}
				}

				allowed = true;

			} else if (character instanceof Hero) {
				Set<Direction> avDirections = new HashSet<Direction>();
				for (Direction direction : currentField.getAvaibleDirections()) {
					avDirections.add(direction);
				}

				character.setDirection(playersDirection);

				if (avDirections.contains(playersDirection)) {
					allowed = true;
				}
			}
		}

		/**
		 * That's one small step for a man, a giant leap for mankind! ~
		 * L.Armstrong
		 */
		if (allowed) {
			character.makeStep();

		}
	}

	/**
	 * Moves hero on the map.
	 * 
	 * @param hero
	 */
	private void moveHero(Hero hero) {
		moveCharacter(hero);

		// Look for the ball
		ContainingField field = (ContainingField) labirynth.getField(
				(int) hero.getCenterPoint().x, (int) hero.getCenterPoint().y);

		// If ball is found, eat it! om nom nom
		if (field.isContainingItem()) { // TODO check item type
			ConsumableItem item = field.getItem();
			this.gameState.addReward(item.getReward());
			// Send to clients
			raiseItemConsumedEvent(field, item);
			field.removeItem();
		}

		// TODO rest of methods body
	}

	private void moveEnemy(Enemy enemy) {
		moveCharacter(enemy);

		// TODO rest of methods body
	}

	/**
	 * Check are the enemy and the hero touching each other.
	 * 
	 * @param hero
	 * @param enemy
	 * @return
	 */
	private boolean checkPositions(Hero hero, Enemy enemy) {
		int minDistX = Math.min(enemy.getWidth(), hero.getWidth());
		int minDistY = Math.min(enemy.getHeight(), hero.getHeight());
		int posDiffX = (int) Math.abs(enemy.getLocX() - hero.getLocX());
		int posDiffY = (int) Math.abs(enemy.getLocY() - hero.getLocY());

		// System.out.println("minDistX=" + minDistX + "; minDistY=" + minDistY
		// + "; posDiffX=" + posDiffX + "; posDiffY=" + posDiffY);

		return posDiffX < minDistX && posDiffY < minDistY;
	}

	/**
	 * Returns the last field where the character was.
	 * 
	 * @param direction
	 * @param currentColumn
	 * @param currentRow
	 * @param fields
	 * @return Field
	 */
	public EnterableField getPreviousField(Direction direction,
			int currentColumn, int currentRow, Field[][] fields) {
		try {
			int fieldWidth = fields[0][0].getWidth();
			int fieldHeight = fields[0][0].getHeight();

			EnterableField prevField = null;
			switch (direction) {
			case Up:
				prevField = (EnterableField) fields[currentColumn][currentRow
						+ fieldHeight];
				break;
			case Down:
				prevField = (EnterableField) fields[currentColumn][currentRow
						- fieldHeight];
				break;
			case Left:
				prevField = (EnterableField) fields[currentColumn + fieldWidth][currentRow];
				break;
			case Right:
				prevField = (EnterableField) fields[currentColumn - fieldWidth][currentRow];
				break;
			default:
				break;
			}
			return prevField;
		} catch (ArrayIndexOutOfBoundsException e) {
			return null;
		}
	}

	// ========== </Methods> ==========

	// ========== <Getters / Setters> ==========
	/**
	 * Actual game state.
	 * 
	 * @return GameState
	 */
	public GameState getGameState() {
		return this.gameState;
	}

	/**
	 * Hero instance.
	 * 
	 * @return
	 */
	public Hero getHero() {
		return this.hero;
	}

	/**
	 * Sets hero direction according to the players choice.
	 * 
	 * @param direction
	 */
	public void setPlayersDirection(Direction direction) {
		if (direction != Direction.Undifined) {
			logger.WriteLine("Player direction changed to: " + direction);
			this.playersDirection = direction;
		}
	}

	// ========== </Getters / Setters> ==========

	// ========== <Constructors> ==========
	public GameController(GameState gameState, Logger logger) {

		this.logger = logger;
		logger.WriteLine("Initilising...", GameController.class.getName());

		this.gameState = gameState;
		this.labirynth = gameState.getMap();
		this.hero = gameState.getHero1();
		this.playersDirection = this.hero.getDirection();

		logger.WriteLine("Initilised!", GameController.class.getName());

	}

	// ========== </Constructors> ==========

	// ========== <Members> ==========
	private final GameState gameState;
	private final Hero hero;
	private Direction playersDirection;
	private final Map labirynth;
	protected Logger logger;
	// ========== </Members> ==========

	// ========== <Contants> ==========

	// ========== </Contants> ==========

}
