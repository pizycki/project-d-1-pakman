package pakman.host.impl;

import pakman.contracts.consumables.ConsumableItem;
import pakman.contracts.host.ItemConsumedEvent;
import pakman.contracts.labyrinth.Field;

public class ItemConsumedEventImpl implements ItemConsumedEvent {

	@Override
	public Field getField() {
		return this.field;
	}

	@Override
	public ConsumableItem getItem() {
		return this.item;
	}

	public ItemConsumedEventImpl(Field field, ConsumableItem item) {
		this.field = field;
		this.item = item;
	}

	private Field field;
	private ConsumableItem item;
}
