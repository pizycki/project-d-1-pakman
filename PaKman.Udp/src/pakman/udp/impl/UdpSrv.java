package pakman.udp.impl;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.List;

import pakman.contracts.udp.MessageReceivedEvent;
import pakman.contracts.udp.MessageReceivedEventListener;
import pakman.contracts.udp.UdpServer;

class UdpSrv extends Thread implements UdpServer {

	// ========== <Methods> ==========

	/**
	 * Awaits for incoming request to the server and handles it. Requests are
	 * welcome on specified port. Whenever request is received, proper event is
	 * fired.
	 */
	protected void handleMessage() {
		try {
			byte[] receiveData = new byte[MAX_BYTE_ARRAY_LENGTH];

			DatagramPacket receivedPacket = new DatagramPacket(receiveData,
					receiveData.length);
			this.serverSocket.receive(receivedPacket);
			String sentence = new String(receivedPacket.getData());
			// System.out.println(sentence);

			// Raise an event, gather replays from listeners
			String[] replays = fireRequestReceivedEvent(sentence);

			if (replays != null && replays.length != 0) {
				// Send replays to clients
				byte[] sendData = new byte[MAX_BYTE_ARRAY_LENGTH];
				InetAddress IPAddress = receivedPacket.getAddress();
				int port = receivedPacket.getPort();
				for (String rep : replays) {
					sendData = rep.getBytes();
					DatagramPacket sendPacket = new DatagramPacket(sendData,
							sendData.length, IPAddress, port);
					serverSocket.send(sendPacket);
				}
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Runs request handling loop.
	 */
	@Override
	public void run() {
		// Infinite loop
		while (true) {
			try {
				handleMessage();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	// ========== </Methods> ==========

	// ========== <Getters / Setters> ==========
	public DatagramSocket getSocket() {
		return serverSocket;
	}

	public int getTimout() {
		return this.timeout;
	}

	// ========== </Getters / Setters> ==========

	// ========== <Events> ==========
	private List<MessageReceivedEventListener> requestReceivedListeners = new ArrayList<MessageReceivedEventListener>();

	public synchronized void addMessageReceivedEventListener(
			MessageReceivedEventListener listener) {
		requestReceivedListeners.add(listener);
	}

	public synchronized void removeMessageReceivedEventListener(
			MessageReceivedEventListener listener) {
		requestReceivedListeners.remove(listener);
	}

	/**
	 * Informs all attached listeners about received request.
	 * 
	 * @param receivedMsg
	 * @return Array of responses from attached listeners.
	 */
	private synchronized String[] fireRequestReceivedEvent(String receivedMsg) {

		List<String> responses = new ArrayList<String>();
		MessageReceivedEvent event = new MessageReceivedEventImpl(this,
				receivedMsg);

		// Inform all listeners about fired event, get responses from each
		// listener and put them into String array.
		try {
			for (MessageReceivedEventListener listener : this.requestReceivedListeners) {
				String response = listener.onMessageReceivedEvent(event);
				if (!response.equals("")) {
					responses.add(response);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			// TODO log error
		}

		// Return responses from listeners
		if (responses.size() > 0) {
			return responses.toArray(new String[0]);
		} else
			return null;
	}

	// ========== </Events> ==========

	// ========== <Constructors> ==========
	public UdpSrv(int port, int timeout) throws SocketException {
		this.timeout = timeout;
		this.serverSocket = new DatagramSocket(port);
	}

	// ========== </Constructors> ==========

	// ========== <Members> ==========
	private DatagramSocket serverSocket;
	private final int timeout;

	// ========== </Members> ==========

	// ========== <Contants> ==========
	public static final int DEFAULT_TIMEOUT = 1000;
	public static final int MAX_BYTE_ARRAY_LENGTH = 1024;
	// ========== </Contants> ==========
}
