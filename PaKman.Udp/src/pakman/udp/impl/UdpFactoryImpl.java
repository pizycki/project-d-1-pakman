package pakman.udp.impl;

import java.net.SocketException;

import pakman.contracts.udp.UdpClient;
import pakman.contracts.udp.UdpFactory;
import pakman.contracts.udp.UdpServer;

public class UdpFactoryImpl implements UdpFactory {

	@Override
	public UdpServer createServer(int port, int timeout) {
		try {
			return new UdpSrv(port, timeout);
		} catch (SocketException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public UdpClient createClient(int port, String host) {
		return new UdpClnt(port, host);
	}
}
