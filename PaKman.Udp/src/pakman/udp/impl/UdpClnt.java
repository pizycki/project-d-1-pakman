package pakman.udp.impl;

import java.io.Closeable;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

import pakman.contracts.udp.UdpClient;
import pakman.contracts.udp.UdpException;

/**
 * Simple UDP client. Maximum message length: 512 characters.
 * 
 * @author Pawe� I�ycki
 * 
 */
class UdpClnt extends Thread implements UdpClient, Closeable {

	public void send(byte[] bytes) throws UdpException, IOException {

		if (bytes.length > MAX_BYTE_ARRAY_LENGTH)
			throw new UdpException("Byte array is to long!");

		// Create socket if necessary.
		if (this.clientSocket == null
				|| (this.clientSocket != null && this.clientSocket.isClosed())) {
			this.clientSocket = new DatagramSocket();
		}

		InetAddress IPAddress = InetAddress.getByName(this.host);
		DatagramPacket sendPacket = new DatagramPacket(bytes, bytes.length,
				IPAddress, this.port);
		clientSocket.send(sendPacket);
	}

	/**
	 * Sends text message using UDP protocol.
	 * 
	 * @param Message
	 * @throws Exception
	 */
	public void send(String msg) throws UdpException, IOException {
		send(msg.getBytes());
	}

	/**
	 * Sends text message using UDP protocol and awaits for response.
	 * 
	 * @throws Exception
	 */
	public String sendAndReceive(String msg) throws UdpException, IOException {
		// Send message
		send(msg);

		// Receive response
		byte[] receiveData = new byte[MAX_BYTE_ARRAY_LENGTH];
		DatagramPacket receivePacket = new DatagramPacket(receiveData,
				receiveData.length);
		clientSocket.receive(receivePacket);
		String modifiedSentence = new String(receivePacket.getData());

		return modifiedSentence;
	}
	
	/**
	 * Closes client socket.
	 */
	@Override
	public void close() throws IOException {
		if (this.clientSocket != null && !clientSocket.isClosed()) {
			this.clientSocket.close();
		}	
	}

	// ========== <Getters / Setters> ==========

	public DatagramSocket getSocket() {
		return this.clientSocket;
	}

	// ========== </Getters / Setters> ==========

	// ========== <Constructors> ==========
	public UdpClnt(int port, String host) {
		this.port = port;
		this.host = host;
	}

	// ========== </Constructors> ==========

	// ========== <Members> ==========
	private final int port;
	private final String host;
	private DatagramSocket clientSocket;

	// ========== </Members> ==========

	// ========== <Contants> ==========
	public static final int MAX_BYTE_ARRAY_LENGTH = 1024;
	// ========== </Contants> ==========



}
