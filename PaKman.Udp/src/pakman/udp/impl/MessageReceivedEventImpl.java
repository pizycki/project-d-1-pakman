package pakman.udp.impl;

import java.util.EventObject;

import pakman.contracts.udp.MessageReceivedEvent;

@SuppressWarnings("serial")
public class MessageReceivedEventImpl extends EventObject implements
		MessageReceivedEvent {

	public String getReceivedMsg() {
		return this.receivedMsg;
	}

	public MessageReceivedEventImpl(Object invoker, String receivedMsg) {
		super(invoker);
		this.receivedMsg = receivedMsg;
	}

	String receivedMsg;
}
