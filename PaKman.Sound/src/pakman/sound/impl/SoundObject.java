package pakman.sound.impl;

import pakman.contracts.sound.Sound;

public class SoundObject implements Sound {

	public SoundObject(String name, String filePath, long length) {
		super();
		this.name = name;
		this.filePath = filePath;
		this.length = length;
	}

	@Override
	public String toString() {
		return "Sound [name=" + name + ", filePath=" + filePath + ", length="
				+ length + "]";
	}

	public String getName() {
		return name;
	}

	public String getFilePath() {
		return filePath;
	}

	public long getLength() {
		return length;
	}

	private String name;
	private String filePath;
	private long length;

}
