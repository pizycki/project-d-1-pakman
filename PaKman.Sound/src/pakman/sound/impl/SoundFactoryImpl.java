package pakman.sound.impl;

import pakman.contracts.sound.Sound;
import pakman.contracts.sound.SoundFactory;

public class SoundFactoryImpl implements SoundFactory {

	@Override
	public Sound create(String name, String filePath, long length) {
		return new pakman.sound.impl.SoundObject(name, filePath, length);
	}

}
