package pakman.sound.impl;

import java.util.HashSet;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import pakman.contracts.sound.Sound;
import pakman.contracts.sound.SoundController;
import pk.glk.soundplayer.ISound;

public class SoundControllerImpl implements SoundController {

	// TODO check if synchronized does not make trouble
	/**
	 * 
	 * @param sound
	 */
	public void playSound(final Sound sound) {
		if (this.playingSounds != null && !this.playingSounds.contains(sound)) {
			this.playingSounds.add(sound);

			// Play sound
			player.PlaySound(sound.getFilePath(), false);

			// Remove sound from the list after it's finished
			Timer timer = new Timer();
			timer.schedule(new TimerTask() {
				@Override
				public void run() {
					playingSounds.remove(sound);
				}
			}, sound.getLength());

			System.out.println("Playing sound: " + sound.toString());
		}
	}
	
	public void stopSound() {
		player.StopPlaying();		
	}

	/**
	 * 
	 * @param player
	 */
	public SoundControllerImpl(ISound player) {
		this.playingSounds = new HashSet<Sound>();
		this.player = player;
	}

	protected Set<Sound> playingSounds;
	protected ISound player;
	

}
