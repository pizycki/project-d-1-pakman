package pakman.messages.impl;

import pakman.common.Direction;
import pakman.contracts.messages.ClientGameUpdateMessage;
import pakman.contracts.messages.MessageType;

public class ClientGameUpdateMessageImpl implements ClientGameUpdateMessage {
	/**
	 * Selected players direction
	 */
	private Direction playerDirection;

	public Direction getPlayerDirection() {
		return playerDirection;
	}

	public void setPlayerDirection(Direction playerDirection) {
		this.playerDirection = playerDirection;
	}

	@Override
	public String toString() {
		return "ClientMessage [playerDirection=" + playerDirection
				+ ", messageType=" + messageType + "]";
	}

	/**
	 * 
	 * @return
	 */
	@Override
	public MessageType getMessageType() {
		return messageType;
	}

	public final MessageType messageType = MessageType.ClientGameUpdate;
}
