package pakman.messages.impl;

import pakman.contracts.messages.HostWaitingForPlayers;
import pakman.contracts.messages.MessageType;

public class HostWaitingForPlayersImpl implements HostWaitingForPlayers {

	/**
	 * 
	 * @return
	 */
	@Override
	public MessageType getMessageType() {
		return messageType;
	}

	private final MessageType messageType = MessageType.HostWaitingForPlayers;

}
