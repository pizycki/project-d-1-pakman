package pakman.messages.impl;

import pakman.contracts.messages.Message;
import pakman.contracts.messages.MessageFactory;
import pakman.contracts.messages.MessageType;

public class MessageFactoryImpl implements MessageFactory {

	public Message create(MessageType msgType) {
		if (msgType != null && msgType != MessageType.Undifined) {
			pakman.messages.impl.GsonConverter converter = new GsonConverter();
			Class<?> clazz = converter.getMessageClass(msgType);

			try {
				return (Message) clazz.newInstance();
			} catch (InstantiationException | IllegalAccessException e) {
				e.printStackTrace();
			}
		}
		return null;
	}
}
