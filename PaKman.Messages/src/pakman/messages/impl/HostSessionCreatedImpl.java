package pakman.messages.impl;

import pakman.contracts.gamestate.GameType;
import pakman.contracts.messages.HostSessionCreated;
import pakman.contracts.messages.MessageType;

public class HostSessionCreatedImpl implements HostSessionCreated {

	public GameType getGameType() {
		return gameType;
	}

	public void setGameType(GameType gameType) {
		this.gameType = gameType;
	}

	private GameType gameType;

	/**
	 * 
	 * @return
	 */
	@Override
	public MessageType getMessageType() {
		return messageType;
	}

	public final MessageType messageType = MessageType.HostSessionCreated;

	@Override
	public String toString() {
		return "HostSessionCreated [gameType=" + gameType + ", messageType="
				+ messageType + "]";
	}

}
