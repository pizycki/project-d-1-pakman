package pakman.messages.impl;

import java.util.Arrays;
import java.util.Map;

import pakman.contracts.messages.JsonConverter;
import pakman.contracts.messages.MessageType;

import com.google.gson.Gson;

public class GsonConverter implements JsonConverter {

	/**
	 * Returns type of the message.
	 * 
	 * @param Raw
	 *            JSON.
	 * @return Message type class.
	 */
	public Class<?> getMessageType(String json) {

		if (!isValid(json))
			json = fixJson(json);

		// Pull out message type from json string
		@SuppressWarnings("rawtypes")
		Map jsonMap = new Gson().fromJson(json, Map.class);
		MessageType mt = (MessageType) MessageType.valueOf((String) jsonMap
				.get(MESSAGE_TYPE_KEY));
		return getMessageClass(mt);

	}

	Class<?> getMessageClass(MessageType type) {
		switch (type) {
		case ClientGameUpdate:
			return ClientGameUpdateMessageImpl.class;
		case HostGameUpdate:
			return HostGameUpdateMessageImpl.class;
		case ClientCreateNewSession:
			return ClientCreateNewSessionImpl.class;
		case HostWaitingForPlayers:
			return HostWaitingForPlayersImpl.class;
		case HostSessionCreated:
			return HostSessionCreatedImpl.class;
		default:
			return null;
		}
	}

	/**
	 * Converts given object into JSON.
	 * 
	 * @param msg
	 * @return
	 */
	public <T> String convertTo(T msg) {
		return gson.toJson(msg, msg.getClass());
	}

	/**
	 * Checks is the JSON valid
	 * 
	 * @param json
	 * @return true/false
	 */
	public boolean isValid(String json) {
		byte[] bytes = json.getBytes();
		for (int i = bytes.length - 1; i >= 0; i--) {
			if (bytes[i] == 0)
				return false;
		}
		return true;
	}

	/**
	 * Converts JSON into object.
	 * 
	 * @param clazz
	 * @param rawJson
	 * @return
	 */
	public <T> T convertFrom(Class<T> clazz, String rawJson) {
		if (!isValid(rawJson))
			rawJson = fixJson(rawJson);
		return (T) gson.fromJson(rawJson, clazz);
	}

	/**
	 * Cuts off the excess leaving plain JSON.
	 * 
	 * @return
	 */
	public String fixJson(String brokenJson) {
		// Removes all non visible characters
		// DONT USE String.replaceAll - IT WON'T WORK
		byte[] bytes = brokenJson.getBytes();
		int offset = 0;
		for (int i = 0; i < bytes.length; i++)
			if (bytes[i] == 0) {
				offset = i;
				break;
			}

		return new String(Arrays.copyOfRange(bytes, 0, offset));
	}

	protected final static Gson gson = new Gson();

	public static final String MESSAGE_TYPE_KEY = "messageType";
}
