package pakman.messages.impl;

import pakman.contracts.gamestate.GameType;
import pakman.contracts.messages.ClientCreateNewSession;
import pakman.contracts.messages.MessageType;

public class ClientCreateNewSessionImpl implements ClientCreateNewSession {

	private GameType gameType;
	private int playerOnePort;
	private int playerTwoPort;

	public GameType getGameType() {
		return gameType;
	}

	public void setGameType(GameType gameType) {
		this.gameType = gameType;
	}

	public int getPlayerOnePort() {
		return playerOnePort;
	}

	public void setPlayerOnePort(int playerOnePort) {
		this.playerOnePort = playerOnePort;
	}

	public int getPlayerTwoPort() {
		return playerTwoPort;
	}

	public void setPlayerTwoPort(int playerTwoPort) {
		this.playerTwoPort = playerTwoPort;
	}

	/**
	 * 
	 * @return
	 */
	@Override
	public MessageType getMessageType() {
		return messageType;
	}

	public final MessageType messageType = MessageType.ClientCreateNewSession;

}
