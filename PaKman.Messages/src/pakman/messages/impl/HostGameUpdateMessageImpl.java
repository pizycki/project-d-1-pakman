package pakman.messages.impl;

import pakman.common.Direction;
import pakman.contracts.gamestate.GameType;
import pakman.contracts.messages.HostGameUpdateMessage;
import pakman.contracts.messages.MessageType;

public class HostGameUpdateMessageImpl implements HostGameUpdateMessage {

	@Override
	public MessageType getMessageType() {
		return messageType;
	}

	public final MessageType messageType = MessageType.HostGameUpdate;

	@Override
	public String toString() {
		return "HostGameUpdateMessage [messageType=" + messageType + ", h1X="
				+ h1X + ", h1Y=" + h1Y + ", h1dir=" + h1dir + ", e1X=" + e1X
				+ ", e1Y=" + e1Y + ", e1dir=" + e1dir + ", e2X=" + e2X
				+ ", e2Y=" + e2Y + ", e2dir=" + e2dir + ", e3X=" + e3X
				+ ", e3Y=" + e3Y + ", e3dir=" + e3dir + ", e4X=" + e4X
				+ ", e4Y=" + e4Y + ", e4dir=" + e4dir + ", consumedItem="
				+ consumedItem + ", itemX=" + itemX + ", itemY=" + itemY
				+ ", playerScore=" + playerScore + ", livesLeft=" + livesLeft
				+ ", heroKilled=" + heroKilled + ", gameOver=" + gameOver
				+ ", gameStarted=" + gameStarted + ", gamePrepared="
				+ gamePrepared + ", gameType=" + gameType + "]";
	}

	public int getH1X() {
		return h1X;
	}

	public void setH1X(int h1x) {
		h1X = h1x;
	}

	public int getH1Y() {
		return h1Y;
	}

	public void setH1Y(int h1y) {
		h1Y = h1y;
	}

	public Direction getH1dir() {
		return h1dir;
	}

	public void setH1dir(Direction h1dir) {
		this.h1dir = h1dir;
	}

	public int getE1X() {
		return e1X;
	}

	public void setE1X(int e1x) {
		e1X = e1x;
	}

	public int getE1Y() {
		return e1Y;
	}

	public void setE1Y(int e1y) {
		e1Y = e1y;
	}

	public Direction getE1dir() {
		return e1dir;
	}

	public void setE1dir(Direction e1dir) {
		this.e1dir = e1dir;
	}

	public int getE2X() {
		return e2X;
	}

	public void setE2X(int e2x) {
		e2X = e2x;
	}

	public int getE2Y() {
		return e2Y;
	}

	public void setE2Y(int e2y) {
		e2Y = e2y;
	}

	public Direction getE2dir() {
		return e2dir;
	}

	public void setE2dir(Direction e2dir) {
		this.e2dir = e2dir;
	}

	public int getE3X() {
		return e3X;
	}

	public void setE3X(int e3x) {
		e3X = e3x;
	}

	public int getE3Y() {
		return e3Y;
	}

	public void setE3Y(int e3y) {
		e3Y = e3y;
	}

	public Direction getE3dir() {
		return e3dir;
	}

	public void setE3dir(Direction e3dir) {
		this.e3dir = e3dir;
	}

	public int getE4X() {
		return e4X;
	}

	public void setE4X(int e4x) {
		e4X = e4x;
	}

	public int getE4Y() {
		return e4Y;
	}

	public void setE4Y(int e4y) {
		e4Y = e4y;
	}

	public Direction getE4dir() {
		return e4dir;
	}

	public void setE4dir(Direction e4dir) {
		this.e4dir = e4dir;
	}

	public boolean isConsumedItem() {
		return consumedItem;
	}

	public void setConsumedItem(boolean consumedItem) {
		this.consumedItem = consumedItem;
	}

	public int getItemX() {
		return itemX;
	}

	public void setItemX(int itemX) {
		this.itemX = itemX;
	}

	public int getItemY() {
		return itemY;
	}

	public void setItemY(int itemY) {
		this.itemY = itemY;
	}

	public int getPlayerScore() {
		return playerScore;
	}

	public void setPlayerScore(int playerScore) {
		this.playerScore = playerScore;
	}

	public int getLivesLeft() {
		return livesLeft;
	}

	public void setLivesLeft(int livesLeft) {
		this.livesLeft = livesLeft;
	}

	public boolean isHeroKilled() {
		return heroKilled;
	}

	public void setHeroKilled(boolean heroKilled) {
		this.heroKilled = heroKilled;
	}

	public boolean isGameOver() {
		return gameOver;
	}

	public void setGameOver(boolean gameOver) {
		this.gameOver = gameOver;
	}

	public boolean isGameStarted() {
		return gameStarted;
	}

	public void setGameStarted(boolean gameStarted) {
		this.gameStarted = gameStarted;
	}

	public boolean isGamePrepared() {
		return gamePrepared;
	}

	public void setGamePrepared(boolean gamePrepared) {
		this.gamePrepared = gamePrepared;
	}

	public GameType getGameType() {
		return gameType;
	}

	public void setGameType(GameType gameType) {
		this.gameType = gameType;
	}

	/*
	 * Hero One X coordinate
	 */
	private int h1X;

	/*
	 * Hero One Y coordinate
	 */
	private int h1Y;

	/**
	 * Direction of Hero One
	 */
	private Direction h1dir;

	/**
	 * Enemy One X coordinate
	 */
	private int e1X;

	/**
	 * Enemy One Y coordinate
	 */
	private int e1Y;

	/**
	 * Enemy One Direction
	 */
	private Direction e1dir;

	/**
	 * Enemy Two X coordinate
	 */
	private int e2X;

	/**
	 * Enemy Two Y coordinate
	 */
	private int e2Y;
	/**
	 * Enemy Two Direction
	 */
	private Direction e2dir;

	/**
	 * Enemy Three X coordinate
	 */
	private int e3X;

	/**
	 * Enemy Three Y coordinate
	 */
	private int e3Y;
	/**
	 * Enemy Three Direction
	 */
	private Direction e3dir;

	/**
	 * Enemy Four X coordinate
	 */
	private int e4X;

	/**
	 * Enemy Four Y coordinate
	 */
	private int e4Y;
	/**
	 * Enemy Four Direction
	 */
	private Direction e4dir;

	/*
	 * Has item been consumed
	 */
	private boolean consumedItem;
	/*
	 * Item X coordinate
	 */
	private int itemX;
	/*
	 * Item Y coordinate
	 */
	private int itemY;

	/**
	 * Player score
	 */
	private int playerScore;

	/**
	 * Remaining number of lives.
	 */
	private int livesLeft;

	/**
	 * True when player dies.
	 */
	private boolean heroKilled;

	/**
	 * True when game is over.
	 */
	private boolean gameOver;

	/**
	 * True when game has begun.
	 */
	private boolean gameStarted;
	/**
	 * True when game is prepared but yet not started.
	 */
	private boolean gamePrepared;

	/*
	 * Singleplayer or multiplayer
	 */
	private GameType gameType;
}
