package pakman.common;

public class DirectionExt 
{
	/**
	 * Gets the opposite direction to given.
	 * @param direction
	 * @return
	 */
	public static Direction getOpposite(Direction direction) {
		switch (direction) {
		case Up:
			return Direction.Down;
		case Down:
			return Direction.Up;
		case Right:
			return Direction.Left;
		case Left:
			return Direction.Right;
		default:
			return Direction.Undifined;
		}
	}
}
