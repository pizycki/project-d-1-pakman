package pakman.database.impl;

import pakman.contracts.database.RecordI;


public class Record implements RecordI{

	// ========== <Members> ==========
	private String nick;
	private int score;
	// ========== </Members> ==========
	
	// ========== <Constructors> ==========
	public Record() { }
	
	public Record(String nick, int score) {
		this.nick = nick;
		this.score = score;
	}
	// ========== </Constructors> ==========
	
	// ========== <Getters / Setters> ==========
	@Override
	public String getNick() {
        return this.nick;
    }
	@Override
	public int getScore() {
		return this.score;
	}
    public void setNick(String nick) {
       this.nick = nick;
    }
    public void setScore(int score) {
		this.score = score;
	}
    // ========== </Getters / Setters> ==========
    
    // ========== <Methods> ==========
    @Override
    public String[] toTable() {
    	String[] table = {this.getNick(), Integer.toString(this.getScore())};
    	return table;
    }
    // ========== </Methods> ==========
}
