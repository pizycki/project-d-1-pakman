package pakman.database.impl;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.swing.JOptionPane;

import pakman.contracts.database.Database;
import pakman.contracts.database.RecordI;

public class DatabaseCore implements Database {

	// ========== <Members> ==========
	private List<RecordI> list = new ArrayList<RecordI>();
	public static DatabaseCore instance = null;
	private Serializer serializer = Serializer.getInstace();
	private Komparator komparator = new Komparator();

	// ========== <Members> ==========

	// ========== <Getters / Setters> ==========
	public List<RecordI> getList() {
		return this.list;
	}

	public void setList(List<RecordI> list) {
		this.list = list;
	}

	// ========== </Getters / Setters> ==========

	// ========== <Methods> ==========
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public void addNewRecord(String nick, int score) {
		Record record = new Record(nick, score);
		list.add(record);
		Collections.sort(new ArrayList(list), komparator);
	}

	@Override
	public void clearRecordList() {
		list.clear();
	}

	@Override
	public void createRecordList() throws FileNotFoundException {
		serializer.Deserialize(DatabaseCore.getInstace());
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public void saveRecordList() throws FileNotFoundException {
		serializer.Serialize(new ArrayList(getList()));
	}

	@Override
	public RecordI getTopRecord() {
		RecordI TopScoreRecord = new Record("nick", 0);
		for (RecordI record : list) {
			if (TopScoreRecord.getScore() < record.getScore()) {
				TopScoreRecord = record;
			}
		}
		return TopScoreRecord;
	}

	public static DatabaseCore getInstace() {
		if (instance == null) {
			instance = new DatabaseCore();
		}
		return instance;
	}

	public void makeRecord(int score) {
		String str = "";
		while (str.equals("")) {
			str = JOptionPane.showInputDialog(null, "Wpisz sw�j nick: ",
					"HIGHSCORE", 1);
			if (str == null) {
				JOptionPane.showMessageDialog(null, "Nie wpisano do rankingu",
						"HIGHSCORE", 1);
				str = "pusty";
			} else if (str.equals("")) {
				JOptionPane.showMessageDialog(null, "Pole nie mo�e by� puste",
						"HIGHSCORE", 1);
				str = "";
			} else if (!str.equals("")) {
				try {
					createRecordList();
					addNewRecord(str, score);
					saveRecordList();
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				}
				JOptionPane.showMessageDialog(null, "Wpisano do rankingu");
			}
			clearRecordList();
		}
	}
	// ========== </Methods> ==========

}
