package pakman.database.impl;

import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;

public class Serializer {

	public static Serializer instance = null;

	// ========== <Methods> ==========
	public void Serialize(ArrayList<Record> highScore)
			throws FileNotFoundException {
		XMLEncoder e = new XMLEncoder(new BufferedOutputStream(
				new FileOutputStream("database/list.xml")));
		e.writeObject(highScore);
		e.close();
	}

	public void Deserialize(DatabaseCore highScore)
			throws FileNotFoundException {
		XMLDecoder d = new XMLDecoder(new BufferedInputStream(
				new FileInputStream("database/list.xml")));
		ArrayList<Record> list = (ArrayList<Record>) d.readObject();
		for (Record record : list) {
			highScore.addNewRecord(record.getNick(), record.getScore());
		}
		d.close();
	}

	public static Serializer getInstace() {
		if (instance == null) {
			instance = new Serializer();
		}
		return instance;
	}
	// ========== </Methods> ==========
}
