package pakman.database.impl;

import java.util.Comparator;

public class Komparator implements Comparator<Record>{
	
	// ========== <Methods> ==========
	@Override
	public int compare(Record r1, Record r2) {	
		if(r2 == null) 
			return -1; 
		if(r1.getScore() > r2.getScore()) 
			return -1; 
		else if(r1.getScore() < r2.getScore()) 
			return 1; 
		else 
			return 0; 
	}
	// ========== </Methods> ==========
}
