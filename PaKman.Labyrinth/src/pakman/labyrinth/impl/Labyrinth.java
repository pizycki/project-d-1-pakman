package pakman.labyrinth.impl;

import pakman.contracts.labyrinth.Field;
import pakman.contracts.labyrinth.Map;

@SuppressWarnings("serial")
public class Labyrinth implements Map {

	// ========== <Methods> ==========

	// ========== </Methods> ==========

	// ========== <Getters / Setters> ==========
	public Field[][] getMapState() {
		return mapState;
	}

	// ========== </Getters / Setters> ==========

	// ========== <Constructors> ==========
	public Labyrinth(Field[][] mapState) {
		this.mapState = mapState;
	}

	// ========== </Constructors> ==========

	// ========== <Members> ==========
	private final Field[][] mapState;

	// ========== </Members> ==========

	@Override
	public int getFieldsRow(int y) {
		// Get height of single cell.
		// LocY of second cell in first column
		int cellHeight = this.mapState[0][1].getLocY();
		int row = Math.abs(y / cellHeight);
		return row;
	}

	@Override
	public int getFieldsColumn(int x) {
		// Get height of single cell.
		// LocX of second cell in first row.
		int cellWidth = this.mapState[1][0].getLocX();
		// Calculate coordinates of cell in fields matrix
		int col = Math.abs(x / cellWidth);
		return col;
	}

	@Override
	public Field getField(int x, int y) {
		try {
			// Calculate coordinates of cell in fields matrix
			int col = getFieldsColumn(x);
			int row = getFieldsRow(y);

			// Return seeked cell
			return this.mapState[col][row];
		} catch (Exception e) {
			return null;
		}
	}
}
