package pakman.labyrinth.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import pakman.common.Direction;
import pakman.contracts.client.ClientException;
import pakman.contracts.consumables.ConsumbleFactory;
import pakman.contracts.labyrinth.EnterableField;
import pakman.contracts.labyrinth.Field;
import pakman.contracts.labyrinth.Map;
import pakman.contracts.labyrinth.MapFactory;

/**
 * This class is for labyrinth producing.
 * 
 * @author Pawe� I�ycki
 * 
 */
public class LabirynthFactory implements MapFactory {
	// ========== <Methods> ==========

	/**
	 * Creates brand new labyrinth.
	 * 
	 * @return
	 */
	public Map create() {

		/*
		 * Najpierw wstaw elementy ograniczaj�ce (ObstacleField) Potem
		 * iteracyjnie wype�nij macierz PathFields korzystaj�c z
		 * getAvaibleDirections. Na koniec usu� niechciane kulki.
		 */

		int horizontalLength = 19;
		int verticalLength = 19;

		Field[][] matrix = new Field[horizontalLength][verticalLength];

		matrix = insertPredifnedObstacles(matrix);

		// Fill matrix with path fields
		for (int colIdx = 0; colIdx < horizontalLength; colIdx++) {
			for (int rowIdx = 0; rowIdx < verticalLength; rowIdx++) {
				Field currentField = matrix[colIdx][rowIdx];
				if (currentField == null) {
					matrix[colIdx][rowIdx] = new PathField(colIdx
							* this.fieldWidth, rowIdx * this.fieldHeight,
							this.fieldWidth, this.fieldHeight, true,
							getAvaibleDirections(rowIdx, colIdx, matrix),
							this.itemsFactory.createBall(DEFAULT_REWARD_POINTS));
				}
			}
		}

		matrix = removeUnpleasedBalls(matrix);

		matrix = blockEnemiesSpawnArea(matrix);

		// matrix = blockTunnels(matrix);

		return new Labyrinth(matrix);
	}

	/**
	 * Looks around the nearest fields seeking for avaible path.
	 * 
	 * @param "Row index of field in field matrix."
	 * @param "Column index of field in field matrix."
	 * @param "Field matrix. Should contain Enterable objects."
	 * @return Set of avaible move directions.
	 */
	protected Set<Direction> getAvaibleDirections(int row, int column,
			Field[][] fields) {
		final Set<Direction> directions = new HashSet<Direction>();

		/*
		 * First arg of array is the column index, second is the row index.
		 */

		// Up direction
		if (row != 0 && !(fields[column][row - 1] instanceof ObstacleField)) {
			directions.add(Direction.Up);
		}

		// Down direction
		if (row != fields[1].length - 1
				&& !(fields[column][row + 1] instanceof ObstacleField)) {
			directions.add(Direction.Down);
		}

		// Left direction
		if (column != 0 && !(fields[column - 1][row] instanceof ObstacleField)) {
			directions.add(Direction.Left);
		}

		// Right direction
		if (column != fields[0].length - 1
				&& !(fields[column + 1][row] instanceof ObstacleField)) {
			directions.add(Direction.Right);
		}

		if (directions.size() == 0)
			return null;
		else
			return directions;
	}

	/**
	 * Inserts walls into labyrinth matrix.
	 * 
	 * @param matrix
	 * @return
	 */
	protected Field[][] insertPredifnedObstacles(Field[][] matrix) {
		List<Integer[]> list = new ArrayList<Integer[]>();

		// Skip row zero
		int r = 0;
		list.add(new Integer[] { 9, r });

		// First row
		r = 1;
		list.add(new Integer[] { 1, r });
		list.add(new Integer[] { 2, r });
		list.add(new Integer[] { 3, r });
		list.add(new Integer[] { 5, r });
		list.add(new Integer[] { 6, r });
		list.add(new Integer[] { 7, r });
		list.add(new Integer[] { 9, r });
		list.add(new Integer[] { 11, r });
		list.add(new Integer[] { 12, r });
		list.add(new Integer[] { 13, r });
		list.add(new Integer[] { 15, r });
		list.add(new Integer[] { 16, r });
		list.add(new Integer[] { 17, r });

		// Skip 2nd row

		// 3rd row
		r = 3;
		list.add(new Integer[] { 1, r });
		list.add(new Integer[] { 2, r });
		list.add(new Integer[] { 3, r });
		list.add(new Integer[] { 5, r });
		list.add(new Integer[] { 7, r });
		list.add(new Integer[] { 8, r });
		list.add(new Integer[] { 9, r });
		list.add(new Integer[] { 10, r });
		list.add(new Integer[] { 11, r });
		list.add(new Integer[] { 13, r });
		list.add(new Integer[] { 15, r });
		list.add(new Integer[] { 16, r });
		list.add(new Integer[] { 17, r });

		// 4th row
		r = 4;
		list.add(new Integer[] { 5, r });
		list.add(new Integer[] { 9, r });
		list.add(new Integer[] { 13, r });

		// 5th row
		r = 5;
		list.add(new Integer[] { 0, r });
		list.add(new Integer[] { 1, r });
		list.add(new Integer[] { 2, r });
		list.add(new Integer[] { 3, r });
		list.add(new Integer[] { 5, r });
		list.add(new Integer[] { 6, r });
		list.add(new Integer[] { 7, r });
		list.add(new Integer[] { 9, r });
		list.add(new Integer[] { 11, r });
		list.add(new Integer[] { 12, r });
		list.add(new Integer[] { 13, r });
		list.add(new Integer[] { 15, r });
		list.add(new Integer[] { 16, r });
		list.add(new Integer[] { 17, r });
		list.add(new Integer[] { 18, r });

		// 6th row
		r = 6;
		list.add(new Integer[] { 0, r });
		list.add(new Integer[] { 1, r });
		list.add(new Integer[] { 2, r });
		list.add(new Integer[] { 3, r });
		list.add(new Integer[] { 5, r });
		list.add(new Integer[] { 13, r });
		list.add(new Integer[] { 15, r });
		list.add(new Integer[] { 16, r });
		list.add(new Integer[] { 17, r });
		list.add(new Integer[] { 18, r });

		// 7th row
		r = 7;
		list.add(new Integer[] { 0, r });
		list.add(new Integer[] { 1, r });
		list.add(new Integer[] { 2, r });
		list.add(new Integer[] { 3, r });
		list.add(new Integer[] { 5, r });
		list.add(new Integer[] { 7, r });
		list.add(new Integer[] { 8, r });
		list.add(new Integer[] { 10, r });
		list.add(new Integer[] { 11, r });
		list.add(new Integer[] { 13, r });
		list.add(new Integer[] { 15, r });
		list.add(new Integer[] { 16, r });
		list.add(new Integer[] { 17, r });
		list.add(new Integer[] { 18, r });

		// 8th row
		r = 8;
		list.add(new Integer[] { 7, r });
		list.add(new Integer[] { 11, r });

		// 9th row
		r = 9;
		list.add(new Integer[] { 0, r });
		list.add(new Integer[] { 1, r });
		list.add(new Integer[] { 2, r });
		list.add(new Integer[] { 3, r });
		list.add(new Integer[] { 5, r });
		list.add(new Integer[] { 7, r });
		list.add(new Integer[] { 8, r });
		list.add(new Integer[] { 9, r });
		list.add(new Integer[] { 10, r });
		list.add(new Integer[] { 11, r });
		list.add(new Integer[] { 13, r });
		list.add(new Integer[] { 15, r });
		list.add(new Integer[] { 16, r });
		list.add(new Integer[] { 17, r });
		list.add(new Integer[] { 18, r });

		// 10th row
		r = 10;
		list.add(new Integer[] { 0, r });
		list.add(new Integer[] { 1, r });
		list.add(new Integer[] { 2, r });
		list.add(new Integer[] { 3, r });
		list.add(new Integer[] { 5, r });
		list.add(new Integer[] { 13, r });
		list.add(new Integer[] { 15, r });
		list.add(new Integer[] { 16, r });
		list.add(new Integer[] { 17, r });
		list.add(new Integer[] { 18, r });

		// 11th row
		r = 11;
		list.add(new Integer[] { 0, r });
		list.add(new Integer[] { 1, r });
		list.add(new Integer[] { 2, r });
		list.add(new Integer[] { 3, r });
		list.add(new Integer[] { 5, r });
		list.add(new Integer[] { 5, r });
		list.add(new Integer[] { 7, r });
		list.add(new Integer[] { 8, r });
		list.add(new Integer[] { 9, r });
		list.add(new Integer[] { 10, r });
		list.add(new Integer[] { 11, r });
		list.add(new Integer[] { 13, r });
		list.add(new Integer[] { 15, r });
		list.add(new Integer[] { 16, r });
		list.add(new Integer[] { 17, r });
		list.add(new Integer[] { 18, r });

		// 12th row
		r = 12;
		list.add(new Integer[] { 9, r });

		// 13th row
		r = 13;
		list.add(new Integer[] { 1, r });
		list.add(new Integer[] { 2, r });
		list.add(new Integer[] { 3, r });
		list.add(new Integer[] { 5, r });
		list.add(new Integer[] { 6, r });
		list.add(new Integer[] { 7, r });
		list.add(new Integer[] { 9, r });
		list.add(new Integer[] { 11, r });
		list.add(new Integer[] { 12, r });
		list.add(new Integer[] { 13, r });
		list.add(new Integer[] { 15, r });
		list.add(new Integer[] { 16, r });
		list.add(new Integer[] { 17, r });

		// 14th row
		r = 14;
		list.add(new Integer[] { 3, r });
		list.add(new Integer[] { 15, r });

		// 15th row
		r = 15;
		list.add(new Integer[] { 0, r });
		list.add(new Integer[] { 1, r });
		list.add(new Integer[] { 3, r });
		list.add(new Integer[] { 5, r });
		list.add(new Integer[] { 7, r });
		list.add(new Integer[] { 8, r });
		list.add(new Integer[] { 9, r });
		list.add(new Integer[] { 10, r });
		list.add(new Integer[] { 11, r });
		list.add(new Integer[] { 13, r });
		list.add(new Integer[] { 15, r });
		list.add(new Integer[] { 17, r });
		list.add(new Integer[] { 18, r });

		// 16th row
		r = 16;
		list.add(new Integer[] { 5, r });
		list.add(new Integer[] { 9, r });
		list.add(new Integer[] { 13, r });

		// 17th row
		r = 17;
		list.add(new Integer[] { 1, r });
		list.add(new Integer[] { 2, r });
		list.add(new Integer[] { 3, r });
		list.add(new Integer[] { 4, r });
		list.add(new Integer[] { 5, r });
		list.add(new Integer[] { 6, r });
		list.add(new Integer[] { 7, r });
		list.add(new Integer[] { 9, r });
		list.add(new Integer[] { 11, r });
		list.add(new Integer[] { 12, r });
		list.add(new Integer[] { 13, r });
		list.add(new Integer[] { 14, r });
		list.add(new Integer[] { 15, r });
		list.add(new Integer[] { 16, r });
		list.add(new Integer[] { 17, r });

		for (Integer[] integers : list) {
			int x = integers[0];
			int y = integers[1];
			matrix[x][y] = new ObstacleField(x * fieldWidth, y * fieldHeight,
					fieldWidth, fieldHeight);
		}

		return matrix;
	}

	/**
	 * Removes balls from cells where they shouldn't appear.
	 * 
	 * @param "Labyrinth matrix."
	 * @return "Updated labyrinth matrix."
	 */
	protected Field[][] removeUnpleasedBalls(Field[][] matrix) {
		List<Integer[]> list = new ArrayList<Integer[]>();

		for (int rowIdx = 5; rowIdx <= 11; rowIdx++)
			for (int colIdx = 5; colIdx <= 13; colIdx++)
				list.add(new Integer[] { rowIdx, colIdx });

		for (Integer[] integers : list) {
			int col = integers[1];
			int row = integers[0];
			Field field = matrix[col][row];
			if (field instanceof PathField) {
				PathField pf = (PathField) field;
				pf.removeItem();
			}
		}

		// Remove balls on the ends of the tunnels
		((PathField) matrix[0][8]).removeItem();
		((PathField) matrix[18][8]).removeItem();

		return matrix;
	}

	/**
	 * Blocks enemies spawn area. No character can enter this area.
	 * 
	 * @param matrix
	 * @return
	 */
	protected Field[][] blockEnemiesSpawnArea(Field[][] matrix) {
		EnterableField field = (EnterableField) matrix[9][6];
		field.removeDirection(Direction.Down);

		// Center-bottom cell
		field = (EnterableField) matrix[9][8];
		field.removeDirection(Direction.Left);
		field.removeDirection(Direction.Right);

		// Center-bottom cell
		field = (EnterableField) matrix[9][7];
		field.removeDirection(Direction.Down);

		return matrix;
	}

	/**
	 * Block tunnels in labyrinth.
	 * 
	 * @param matrix
	 * @return
	 */
	protected Field[][] blockTunnels(Field[][] matrix) {
		EnterableField leftEntrence = (EnterableField) matrix[4][8];
		leftEntrence.removeDirection(Direction.Left);
		EnterableField rightEntrence = (EnterableField) matrix[14][8];
		rightEntrence.removeDirection(Direction.Right);
		return matrix;
	}

	// ========== </Methods> ==========

	// ========== <Getters / Setters> ==========

	// ========== </Getters / Setters> ==========

	// ========== <Constructors> ==========
	public LabirynthFactory(int fieldWidth, int fieldHeight,
			ConsumbleFactory itemsFactory) throws ClientException {
		this.fieldHeight = fieldHeight;
		this.fieldWidth = fieldWidth;

		// Inject items factory
		if (itemsFactory == null)
			throw new ClientException("Items factory is be null!");
		this.itemsFactory = itemsFactory;
		// if (CACHE_ENABLED) {
		// this.cachedFields = new HashMap<Set<Direction>, Field>();
		// }
	}

	// ========== </Constructors> ==========

	// ========== <Members> ==========
	private int fieldWidth;
	private int fieldHeight;
	protected ConsumbleFactory itemsFactory;

	// protected final Map<Set<Direction>, Field> cachedFields;

	// ========== </Members> ==========

	// ========== <Contants> ==========
	// public static final boolean CACHE_ENABLED = true;
	public static final int DEFAULT_REWARD_POINTS = 100;
	// ========== </Contants> ==========
}
