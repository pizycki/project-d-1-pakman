package pakman.labyrinth.impl;

import pakman.contracts.labyrinth.Field;

public final class FieldHelper {

	/**
	 * Returns field containing given point.
	 * 
	 * @param "Horizontal coordinate"
	 * @param "Vertical coordinate"
	 * @return Field
	 */
	public static Field getField(int x, int y, Field[][] fields) {
		try {
			// Calculate coordinates of cell in fields matrix
			int col = getFieldsColumn(x, fields);
			int row = getFieldsRow(y, fields);

			// Return seeked cell
			return fields[col][row];
		} catch (Exception e) {
			return null;
		}
	}

	public static int getFieldsRow(int y, Field[][] fields) {
		// Get height of single cell.
		// LocY of second cell in first column
		int cellHeight = fields[0][1].getLocY();
		int row = Math.abs(y / cellHeight);
		return row;
	}

	public static int getFieldsColumn(int x, Field[][] fields) {
		// Get height of single cell.
		// LocX of second cell in first row.
		int cellWidth = fields[1][0].getLocX();
		// Calculate coordinates of cell in fields matrix
		int col = Math.abs(x / cellWidth);
		return col;
	}
}
