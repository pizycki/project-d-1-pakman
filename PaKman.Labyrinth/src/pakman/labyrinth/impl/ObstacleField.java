package pakman.labyrinth.impl;

import java.awt.Point;
import java.awt.geom.Point2D;

import pakman.contracts.labyrinth.Field;

@SuppressWarnings("serial")
public class ObstacleField implements Field {

	@Override
	public String toString() {
		return "ObstacleField [location=" + location + ", width=" + width
				+ ", height=" + height + "]";
	}

	// ========== </Methods> ==========

	// ========== <Getters / Setters> ==========
	@Override
	public Point getLocation() {
		return this.location;
	}

	@Override
	public int getLocX() {
		return this.location.x;
	}

	@Override
	public int getLocY() {
		return this.location.y;
	}

	@Override
	public int getWidth() {
		return this.width;
	}

	@Override
	public int getHeight() {
		return this.height;
	}

	@Override
	public Point getCenterPoint() {
		double midX = (double) (getLocX() + getWidth() / 2);
		double midY = (double) (getLocY() + getHeight() / 2);
		Point2D centerPoint = new Point();
		centerPoint.setLocation(midX, midY);
		return (Point) centerPoint;
	}

	// ========== </Getters / Setters> ==========

	// ========== <Constructors> ==========
	public ObstacleField(int x, int y, int width, int height) {
		this.location = new Point(x, y);
		this.width = width;
		this.height = height;
	}

	// ========== </Constructors> ==========

	// ========== <Members> ==========

	private Point location;
	private int width;
	private int height;

	// ========== </Members> ==========

}
