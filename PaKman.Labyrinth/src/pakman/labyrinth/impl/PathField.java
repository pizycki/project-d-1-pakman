package pakman.labyrinth.impl;

import java.awt.Point;
import java.awt.geom.Point2D;
import java.util.Set;

import pakman.common.Direction;
import pakman.contracts.consumables.ConsumableItem;
import pakman.contracts.labyrinth.ContainingField;
import pakman.contracts.labyrinth.EnterableField;

@SuppressWarnings("serial")
public class PathField implements EnterableField, ContainingField {

	// ========== <Methods> ==========
	public boolean canGo(Direction direction) {
		if (direction.equals(Direction.Undifined))
			return false;
		return this.directions.contains(direction);
	}

	@Override
	public String toString() {
		return "PathField [location=" + location + ", directions=" + directions
				+ ", width=" + width + ", height=" + height + ", item=" + item
				+ "]";
	}

	// ========== </Methods> ==========

	// ========== <Getters / Setters> ==========
	@Override
	public boolean isContainingItem() {
		return this.item != null;
	}

	@Override
	public void removeItem() {
		this.item = null;
	}

	@Override
	public boolean canGoUp() {
		return canGo(Direction.Up);
	}

	@Override
	public boolean canGoRight() {
		return canGo(Direction.Right);
	}

	@Override
	public boolean canGoDown() {
		return canGo(Direction.Down);
	}

	@Override
	public boolean canGoLeft() {
		return canGo(Direction.Left);
	}

	@Override
	public Point getLocation() {
		return this.location;
	}

	@Override
	public int getLocX() {
		return this.location.x;
	}

	@Override
	public int getLocY() {
		return this.location.y;
	}

	@Override
	public int getWidth() {
		return this.width;
	}

	@Override
	public int getHeight() {
		return this.height;
	}

	@Override
	public ConsumableItem getItem() {
		return this.item;
	}

	@Override
	public Point getCenterPoint() {
		double midX = (double) (getLocX() + getWidth() / 2);
		double midY = (double) (getLocY() + getHeight() / 2);
		Point2D centerPoint = new Point();
		centerPoint.setLocation(midX, midY);
		return (Point) centerPoint;
	}

	@Override
	public Direction[] getAvaibleDirections() {
		return this.directions.toArray(new Direction[this.directions.size()]);
	}

	@Override
	public void removeDirection(Direction direction) {
		if (direction != Direction.Undifined
				&& this.directions.contains(direction)) {
			this.directions.remove(direction);
		}
	}

	// ========== </Getters / Setters> ==========

	// ========== <Constructors> ==========
	public PathField(int x, int y, int width, int height, boolean enterable,
			Set<Direction> directions, ConsumableItem item) {
		this.location = new Point(x, y);
		this.width = width;
		this.height = height;
		this.directions = directions;
		this.item = item;
	}

	// ========== </Constructors> ==========

	// ========== <Members> ==========
	private Point location;
	private final Set<Direction> directions;
	private int width;
	private int height;
	private ConsumableItem item;

	// ========== </Members> ==========
}
