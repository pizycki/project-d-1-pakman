package pakman.GUI;

import java.util.HashSet;
import java.util.Set;

import pakman.common.Direction;
import pakman.contracts.database.Database;
import pakman.contracts.gamestate.GameState;
import pakman.contracts.gui.Gui;
import pakman.contracts.gui.NewGameSelectedEventListener;
import pakman.contracts.gui.PlayerDirectionSelectEventListener;
import pakman.contracts.sound.SoundController;
import pakman.contracts.sound.SoundFactory;

/**
 * This class controls behavior of Graphical User Interface.
 * 
 * @author Pawe�
 * 
 */
public class GuiController implements Gui {

	// ========== <Events> ==========

	// ========== <Events - NewGameSelectedEvent> ==========
	private Set<NewGameSelectedEventListener> newGameSelectedEventListeners = new HashSet<NewGameSelectedEventListener>();

	public void addNewGameSelectedEventListener(
			NewGameSelectedEventListener listener) {
		if (this.newGameSelectedEventListeners != null) {
			this.newGameSelectedEventListeners.add(listener);
		}
	}

	public void removeNewGameSelectedEventListener(
			NewGameSelectedEventListener listener) {
		if (this.newGameSelectedEventListeners != null
				&& this.newGameSelectedEventListeners.size() != 0) {
			this.newGameSelectedEventListeners.remove(listener);
		}
	}

	private void raiseNewGameSelectedEvent() {
		for (NewGameSelectedEventListener listener : this.newGameSelectedEventListeners) {
			listener.onNewGameSelectedEvent();
		}
	}

	// ========== </Events - NewGameSelectedEvent> ==========

	// ========== <Events - PlayerDirectionSelectEventListener> ==========

	private Set<PlayerDirectionSelectEventListener> playerDirectionSelectEventListeners = new HashSet<PlayerDirectionSelectEventListener>();

	public void addPlayerDirectionSelectEventListener(
			PlayerDirectionSelectEventListener listener) {
		if (this.playerDirectionSelectEventListeners != null) {
			this.playerDirectionSelectEventListeners.add(listener);
		}
	}

	public void removePlayerDirectionSelectEventListener(
			PlayerDirectionSelectEventListener listener) {
		if (this.playerDirectionSelectEventListeners != null
				&& this.playerDirectionSelectEventListeners.size() != 0) {
			this.playerDirectionSelectEventListeners.remove(listener);
		}
	}

	private void raisePlayerDirectionSelectEvent(Direction direction) {
		for (PlayerDirectionSelectEventListener listener : this.playerDirectionSelectEventListeners) {
			listener.onPlayerDirectionSelectEvent(direction);
		}
	}

	// ========== </Events - PlayerDirectionSelectEventListener> ==========

	// ========== </Events> ==========

	private GameFrame gameFrame;

	public GuiController(String frameTitle, SoundController soundController,
			SoundFactory soundsFactory, Database database) {
		this.gameFrame = new GameFrame(this, frameTitle, database);
	}

	@Override
	public void show() {
		if (this.gameFrame != null) {
			this.gameFrame.setVisible(true);
		}
	}

	@Override
	public void prepareNewGame(GameState gameState) {
		this.gameFrame.loadNewGameWindow(gameState);
	}

	public void newGameSelected() {
		raiseNewGameSelectedEvent();
	}

	public void changePlayerDirection(Direction direction) {
		if (direction != null || direction != Direction.Undifined) {
			raisePlayerDirectionSelectEvent(direction);
		}
	}

	@Override
	public void updateGame() {
		this.gameFrame.repaintGameWindow();
	}

	@Override
	public void showMenuWindow() {
		this.gameFrame.switchToMenu();
	}
}
