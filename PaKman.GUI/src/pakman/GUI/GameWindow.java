package pakman.GUI;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.util.Set;

import javax.swing.JPanel;

import pakman.common.Direction;
import pakman.contracts.characters.Enemy;
import pakman.contracts.characters.Hero;
import pakman.contracts.gamestate.GameState;
import pakman.contracts.gamestate.GameType;
import pakman.contracts.gui.GuiException;
import pakman.contracts.labyrinth.ContainingField;
import pakman.contracts.labyrinth.EnterableField;
import pakman.contracts.labyrinth.Field;

@SuppressWarnings("serial")
public class GameWindow extends JPanel {

	// ========== <Methods> ==========
	/**
	 * Paints the window.
	 */
	@Override
	public void paint(Graphics g) {
		super.paint(g);

		// Paint map
		paintMap(g);

		// Draw red rectangle around all the window
		if (DRAW_GAME_WINDOW_RED_RECTANGLE) {
			g.setColor(Color.RED);
			g.drawRect(0, 0, WIDTH, HEIGHT);
		}

		// Paint fields on the map
		paintFields(g, this.gameState.getMap().getMapState());

		// Paint cell borders
		if (DRAW_ENTERABLE_FIELD_AVAIBLE_EXITS)
			paintBorders((Graphics2D) g, this.gameState.getMap().getMapState());

		// Paint portals
		paintPortals(g);

		// Paint hero on the map
		paintHero(g, this.gameState.getHero1());
		// Paint all enemies
		Set<Enemy> enemiesSet = this.gameState.getEnemies();
		Enemy[] enemiesArr = (Enemy[]) enemiesSet.toArray(new Enemy[enemiesSet
				.size()]);
		if (enemiesArr.length != 0) {
			paintEnemies(g, enemiesArr, this.enemiesPaintingOrder);
		}

		// If game is over, draw appropriate text on screen.
		if (this.gameState.isGameOver()) {
			showTextMessage((Graphics2D) g, GAME_OVER_SPLASH_TEXT, 110);
		}

		if (this.gameState.getGameType() == GameType.Singleplayer
				&& !this.gameState.isGameStarted()) {
			// Show text "Waiting for players" on the screen.
			showTextMessage((Graphics2D) g, GET_READY_SPLASH_TEXT, 150);
		} else if (this.gameState.getGameType() == GameType.Multiplayer) {
			// Show text "Waiting for players" on the screen.
			showTextMessage((Graphics2D) g, WAITING_FOR_PLAYERS_SPLASH_TEXT,
					120);
		}
	}

	/**
	 * Paints fields in the window.
	 * 
	 * @param g
	 */
	public void paintFields(Graphics g, Field[][] mapState) {

		Graphics2D g2d = (Graphics2D) g;
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);
		g2d.setPaint(Color.BLUE);

		Field currentField = null;
		for (int i = 0; i < mapState[0].length; i++) {
			for (int j = 0; j < mapState[1].length; j++) {
				currentField = mapState[i][j];
				if (currentField instanceof ContainingField) {
					if (((ContainingField) currentField).isContainingItem()) {
						g.drawImage(ImagesRepository.getBallImage(),
								currentField.getLocX() + LEFT_MARGIN,
								currentField.getLocY() + TOP_MARGIN, null);
					}
				} else {
					if (DRAW_OBSTACLES) {
						g2d.fill(new Rectangle(currentField.getLocX(),
								currentField.getLocY(), FIELD_WIDTH,
								FIELD_HEIGHT));
					}
				}
			}
		}
	}

	/**
	 * Paints text with message on semi black rectangle in the middle-top-center
	 * of the window.
	 * 
	 * @param Graphics
	 * @param message
	 * @param textStartX
	 */
	public void showTextMessage(Graphics2D g, String message, int textStartX) {

		Color tmp = g.getColor();
		// Draw semi black filled rectangle
		Color semiBlack = new Color(0, 0f, 0f, 0.8f);
		g.setColor(semiBlack);
		g.fill(new Rectangle(100, 100, 300, 50));

		g.setColor(Color.YELLOW);
		Font font = new Font("Arial", Font.PLAIN, 30);
		g.setFont(font);
		g.drawString(message, textStartX, 140);
		g.setColor(tmp);
	}

	/**
	 * Paints given character on the map.
	 * 
	 * @param g
	 * @param c
	 * @param img
	 */
	public void paintCharacter(Graphics g,
			pakman.contracts.characters.Character c, Image img) {
		int x = (int) c.getLocX();
		int y = (int) c.getLocY();

		g.drawImage(img, x, y, FIELD_WIDTH, FIELD_HEIGHT, null);
	}

	/**
	 * Paints hero on the map.
	 * 
	 * @param g
	 * @param hero
	 */
	public void paintHero(Graphics g, Hero hero) {

		// Set hero mouth state (open/close)
		boolean mouthOpen = true;
		int doubledHerosWidth = hero.getWidth() + hero.getWidth();
		int doubledHerosHeight = hero.getHeight() + hero.getHeight();
		if (hero.getDirection() == Direction.Up
				|| hero.getDirection() == Direction.Down) {
			mouthOpen = hero.getLocY() % doubledHerosHeight < hero.getHeight();
		} else {
			mouthOpen = hero.getLocX() % doubledHerosWidth > hero.getWidth();
		}

		// Get appropriate hero image and paint hero
		Image heroImage = ImagesRepository.getHeroImage(hero.getDirection(),
				mouthOpen);
		paintCharacter(g, hero, heroImage);
	}

	/**
	 * Paints an enemy on the map.
	 * 
	 * @param g
	 * @param enemy
	 * @param enemyNumber
	 */
	public void paintEnemy(Graphics g, Enemy enemy,
			ImagesRepository.EnemyNumber enemyNumber) {
		// Get appropriate enemy image and paint enemy
		Image enemyImage = ImagesRepository.getEnemyImage(enemyNumber,
				enemy.getDirection());
		paintCharacter(g, enemy, enemyImage);
	}

	/**
	 * Paints all given enemies on the map.
	 * 
	 * @param g
	 * @param enemies
	 * @param paintingOrder
	 */
	public void paintEnemies(Graphics g, Enemy[] enemies,
			ImagesRepository.EnemyNumber[] paintingOrder) {
		if (enemies.length == paintingOrder.length && enemies.length != 0) {
			for (int i = 0; i < enemies.length; i++) {
				Enemy enemy = enemies[i];
				ImagesRepository.EnemyNumber enemyNumber = paintingOrder[i];
				paintEnemy(g, enemy, enemyNumber);
			}

		}
	}

	/**
	 * Paints portal on the map
	 * 
	 * @param g
	 */
	public void paintPortals(Graphics g) {
		g.drawImage(ImagesRepository.LEFT_ORANGE_PORTAL, 0, 200, FIELD_WIDTH,
				FIELD_HEIGHT, null);
		g.drawImage(ImagesRepository.RIGHT_BLUE_PORTAL, 450, 200, FIELD_WIDTH,
				FIELD_HEIGHT, null);
	}

	/**
	 * Paint borders around enterable fields indicating which ways are open.
	 * 
	 * @param g2d
	 * @param matrix
	 */
	public void paintBorders(Graphics2D g2d, Field[][] matrix) {
		EnterableField field = null;

		for (int colIdx = 0; colIdx < matrix[0].length; colIdx++)
			for (int rowIdx = 0; rowIdx < matrix[1].length; rowIdx++)
				if (matrix[colIdx][rowIdx] instanceof EnterableField) {
					field = (EnterableField) matrix[colIdx][rowIdx];
					paintCellBorders(g2d, field, rowIdx, colIdx);
				}
	}

	public void paintMap(Graphics g) {
		g.drawImage(ImagesRepository.MAP, 0, 0, WIDTH, HEIGHT, null);
	}

	/**
	 * 
	 * @param g2d
	 * @param field
	 * @param row
	 * @param col
	 */
	public void paintCellBorders(Graphics2D g2d, EnterableField field, int row,
			int col) {
		Color storedColor = g2d.getColor();
		g2d.setColor(Color.GREEN);

		int x1;
		int y1;
		int x2;
		int y2;

		// Top border
		if (!field.canGoUp()) {
			x1 = col * field.getWidth();
			y1 = row * field.getHeight();
			x2 = (col + 1) * field.getWidth() - 1;
			y2 = row * field.getHeight();

			g2d.drawLine(x1, y1, x2, y2);
		}

		// Bottom border
		if (!field.canGoDown()) {
			x1 = col * field.getWidth();
			y1 = (row + 1) * field.getHeight() - 1;
			x2 = (col + 1) * field.getWidth() - 1;
			y2 = (row + 1) * field.getHeight() - 1;

			g2d.drawLine(x1, y1, x2, y2);
		}

		// Left border
		if (!field.canGoLeft()) {
			x1 = col * field.getWidth();
			y1 = row * field.getHeight();
			x2 = col * field.getWidth();
			y2 = (row + 1) * field.getHeight() - 1;

			g2d.drawLine(x1, y1, x2, y2);
		}

		// Right border
		if (!field.canGoRight()) {
			x1 = (col + 1) * field.getWidth() - 1;
			y1 = row * field.getHeight();
			x2 = (col + 1) * field.getWidth() - 1;
			y2 = (row + 1) * field.getHeight() - 1;

			g2d.drawLine(x1, y1, x2, y2);
		}

		g2d.setColor(storedColor);
	}

	// ========== </Methods> ==========

	// ========== <Getters / Setters> ==========

	// ========== </Getters / Setters> ==========

	// ========== <Constructors> ==========
	public GameWindow(GameState gameState, boolean drawRedRect,
			boolean drawEnterableFields, boolean drawObstacles)
			throws GuiException {
		if (gameState == null)
			throw new GuiException("Game state is null!");
		this.gameState = gameState;

		GameWindow.DRAW_ENTERABLE_FIELD_AVAIBLE_EXITS = drawEnterableFields;
		GameWindow.DRAW_GAME_WINDOW_RED_RECTANGLE = drawEnterableFields;
		GameWindow.DRAW_OBSTACLES = drawObstacles;

		setSize(WIDTH, HEIGHT);
		setBackground(Color.GRAY);

		// Set enemies painting order
		this.enemiesPaintingOrder = new ImagesRepository.EnemyNumber[4];
		this.enemiesPaintingOrder[0] = ImagesRepository.EnemyNumber.One;
		this.enemiesPaintingOrder[1] = ImagesRepository.EnemyNumber.Two;
		this.enemiesPaintingOrder[2] = ImagesRepository.EnemyNumber.Three;
		this.enemiesPaintingOrder[3] = ImagesRepository.EnemyNumber.Four;

	}

	public GameWindow(GameState gameState) throws GuiException {
		this(gameState, false, false, false);
	}

	// ========== </Constructors> ==========

	// ========== <Members> ==========
	private GameState gameState;
	protected ImagesRepository.EnemyNumber[] enemiesPaintingOrder;
	// ========== </Members> ==========

	// ========== <Constants> ==========
	public static final int FIELD_WIDTH = 25;
	public static final int FIELD_HEIGHT = 25;
	public static final int LEFT_MARGIN = 7;
	public static final int TOP_MARGIN = 7;
	public static final int WIDTH = 475;
	public static final int HEIGHT = 475;

	private static boolean DRAW_GAME_WINDOW_RED_RECTANGLE;
	private static boolean DRAW_ENTERABLE_FIELD_AVAIBLE_EXITS;
	private static boolean DRAW_OBSTACLES;

	public static final String GAME_OVER_SPLASH_TEXT = "Game over";
	public static final String GET_READY_SPLASH_TEXT = "Get ready !";
	public static final String WAITING_FOR_PLAYERS_SPLASH_TEXT = "Waiting for players...";
	// ========== </Constants> ==========

}
