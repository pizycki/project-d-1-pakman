package pakman.GUI;

import java.awt.Image;

import javax.swing.ImageIcon;

import pakman.common.Direction;

final class ImagesRepository {

	public enum EnemyNumber {
		One, Two, Three, Four
	};

	// ========== <Methods> ==========
	public static Image getHeroImage(Direction direction, boolean open) {
		Image image = null;

		switch (direction) {
		case Up:
			if (open)
				image = PAKMAN_UP_OPEN;
			else
				image = PAKMAN_UP_CLOSE;

			break;
		case Right:
			if (open)
				image = PAKMAN_RIGHT_OPEN;

			else
				image = PAKMAN_RIGHT_CLOSE;

			break;
		case Down:
			if (open)
				image = PAKMAN_DOWN_OPEN;

			else
				image = PAKMAN_DOWN_CLOSE;

			break;
		case Left:
			if (open)
				image = PAKMAN_LEFT_OPEN;

			else
				image = PAKMAN_LEFT_CLOSE;

			break;
		default:
			break;
		}

		return image;
	}

	public static Image getEnemyImage(EnemyNumber enemyNumber,
			Direction direction) {
		switch (enemyNumber) {
		case One:
			return getEnemyOneImage(direction);
		case Two:
			return getEnemyTwoImage(direction);
		case Three:
			return getEnemyThreeImage(direction);
		case Four:
			return getEnemyFourImage(direction);
		}
		return null;
	}

	public static Image getEnemyOneImage(Direction direction) {
		Image image = null;

		switch (direction) {
		case Up:
			image = PIKEJ_UP_DOWN;
			break;
		case Right:
			image = PIKEJ_RIGHT;
			break;
		case Down:
			image = PIKEJ_UP_DOWN;
			break;
		case Left:
			image = PIKEJ_LEFT;
			break;
		default:
			break;
		}
		return image;
	}

	public static Image getEnemyTwoImage(Direction direction) {
		Image image = null;

		switch (direction) {
		case Up:
			image = HOMER_UP_DOWN;
			break;
		case Right:
			image = HOMER_RIGHT;
			break;
		case Down:
			image = HOMER_UP_DOWN;
			break;
		case Left:
			image = HOMER_LEFT;
			break;
		default:
			break;
		}
		return image;
	}

	public static Image getEnemyThreeImage(Direction direction) {
		Image image = null;

		switch (direction) {
		case Up:
			image = HANNIBAL_UP_DOWN;
			break;
		case Right:
			image = HANNIBAL_RIGHT;
			break;
		case Down:
			image = HANNIBAL_UP_DOWN;
			break;
		case Left:
			image = HANNIBAL_LEFT;
			break;
		default:
			break;
		}
		return image;
	}

	public static Image getEnemyFourImage(Direction direction) {
		Image image = null;

		switch (direction) {
		case Up:
			image = RASTA_UP_DOWN;
			break;
		case Right:
			image = RASTA_RIGHT;
			break;
		case Down:
			image = RASTA_UP_DOWN;
			break;
		case Left:
			image = RASTA_LEFT;
			break;
		default:
			break;
		}
		return image;
	}

	public static Image getPassiveEnemyImage(Direction direction) {
		Image image = null;

		switch (direction) {
		case Up:
			image = PASSIVE_UP_DOWN;
			break;
		case Right:
			image = PASSIVE_RIGHT;
			break;
		case Down:
			image = PASSIVE_UP_DOWN;
			break;
		case Left:
			image = PASSIVE_LEFT;
			break;
		default:
			break;
		}
		return image;
	}

	public static Image getBallImage() {
		return BALL;
	}

	// ========== </Methods> ==========

	// ========== <Contants> ==========

	/* Miscellaneous images */
	public static final String BALL_URL = "img/misc/ball.png";
	public static final Image BALL = new ImageIcon(BALL_URL).getImage();

	/* Pakman images */
	public static final String PAKMAN_RIGHT_OPEN_URL = "img/pakman/pakman_right_open.png";
	public static final String PAKMAN_RIGHT_CLOSE_URL = "img/pakman/pakman_right_close.png";
	public static final String PAKMAN_LEFT_OPEN_URL = "img/pakman/pakman_left_open.png";
	public static final String PAKMAN_LEFT_CLOSE_URL = "img/pakman/pakman_left_close.png";
	public static final String PAKMAN_UP_OPEN_URL = "img/pakman/pakman_up_open.png";
	public static final String PAKMAN_UP_CLOSE_URL = "img/pakman/pakman_up_close.png";
	public static final String PAKMAN_DOWN_OPEN_URL = "img/pakman/pakman_down_open.png";
	public static final String PAKMAN_DOWN_CLOSE_URL = "img/pakman/pakman_down_close.png";
	public static final Image PAKMAN_RIGHT_OPEN = new ImageIcon(
			PAKMAN_RIGHT_OPEN_URL).getImage();
	public static final Image PAKMAN_RIGHT_CLOSE = new ImageIcon(
			PAKMAN_RIGHT_CLOSE_URL).getImage();
	public static final Image PAKMAN_LEFT_OPEN = new ImageIcon(
			PAKMAN_LEFT_OPEN_URL).getImage();
	public static final Image PAKMAN_LEFT_CLOSE = new ImageIcon(
			PAKMAN_LEFT_CLOSE_URL).getImage();
	public static final Image PAKMAN_UP_OPEN = new ImageIcon(PAKMAN_UP_OPEN_URL)
			.getImage();
	public static final Image PAKMAN_UP_CLOSE = new ImageIcon(
			PAKMAN_UP_CLOSE_URL).getImage();
	public static final Image PAKMAN_DOWN_OPEN = new ImageIcon(
			PAKMAN_DOWN_OPEN_URL).getImage();
	public static final Image PAKMAN_DOWN_CLOSE = new ImageIcon(
			PAKMAN_DOWN_CLOSE_URL).getImage();

	/* Ghosts images */

	/* Pikej Ghost */
	public static final String PIKEJ_UP_DOWN_URL = "img/pikejGhost/pikej_up_down.png";
	public static final String PIKEJ_RIGHT_URL = "img/pikejGhost/pikej_right.png";
	public static final String PIKEJ_LEFT_URL = "img/pikejGhost/pikej_left.png";
	public static final Image PIKEJ_UP_DOWN = new ImageIcon(PIKEJ_UP_DOWN_URL)
			.getImage();
	public static final Image PIKEJ_RIGHT = new ImageIcon(PIKEJ_RIGHT_URL)
			.getImage();
	public static final Image PIKEJ_LEFT = new ImageIcon(PIKEJ_LEFT_URL)
			.getImage();

	/* Homer Ghost */
	public static final String HOMER_UP_DOWN_URL = "img/homerGhost/homer_up_down.png";
	public static final String HOMER_RIGHT_URL = "img/homerGhost/homer_right.png";
	public static final String HOMER_LEFT_URL = "img/homerGhost/homer_left.png";
	public static final Image HOMER_UP_DOWN = new ImageIcon(HOMER_UP_DOWN_URL)
			.getImage();
	public static final Image HOMER_RIGHT = new ImageIcon(HOMER_RIGHT_URL)
			.getImage();
	public static final Image HOMER_LEFT = new ImageIcon(HOMER_LEFT_URL)
			.getImage();

	/* Hannibal Ghost */
	public static final String HANNIBAL_UP_DOWN_URL = "img/hannibalGhost/hannibal_up_down.png";
	public static final String HANNIBAL_RIGHT_URL = "img/hannibalGhost/hannibal_right.png";
	public static final String HANNIBAL_LEFT_URL = "img/hannibalGhost/hannibal_left.png";
	public static final Image HANNIBAL_UP_DOWN = new ImageIcon(
			HANNIBAL_UP_DOWN_URL).getImage();
	public static final Image HANNIBAL_RIGHT = new ImageIcon(HANNIBAL_RIGHT_URL)
			.getImage();
	public static final Image HANNIBAL_LEFT = new ImageIcon(HANNIBAL_LEFT_URL)
			.getImage();

	/* Rasta Ghost */
	public static final String RASTA_UP_DOWN_URL = "img/rastaGhost/rasta_up_down.png";
	public static final String RASTA_RIGHT_URL = "img/rastaGhost/rasta_right.png";
	public static final String RASTA_LEFT_URL = "img/rastaGhost/rasta_left.png";
	public static final Image RASTA_UP_DOWN = new ImageIcon(RASTA_UP_DOWN_URL)
			.getImage();
	public static final Image RASTA_RIGHT = new ImageIcon(RASTA_RIGHT_URL)
			.getImage();
	public static final Image RASTA_LEFT = new ImageIcon(RASTA_LEFT_URL)
			.getImage();

	/* Passive Ghost */
	public static final String PASSIVE_UP_DOWN_URL = "img/passiveGhost/passive_up_down.png";
	public static final String PASSIVE_RIGHT_URL = "img/passiveGhost/passive_right.png";
	public static final String PASSIVE_LEFT_URL = "img/passiveGhost/passive_left.png";
	public static final Image PASSIVE_UP_DOWN = new ImageIcon(
			PASSIVE_UP_DOWN_URL).getImage();
	public static final Image PASSIVE_RIGHT = new ImageIcon(PASSIVE_RIGHT_URL)
			.getImage();
	public static final Image PASSIVE_LEFT = new ImageIcon(PASSIVE_LEFT_URL)
			.getImage();

	/* Portals */
	public static final String LEFT_ORANGE_PORTAL_URL = "img/portals/orange_left_portal.png";
	public static final String RIGHT_BLUE_PORTAL_URL = "img/portals/blue_right_portal.png";
	public static final Image LEFT_ORANGE_PORTAL = new ImageIcon(
			LEFT_ORANGE_PORTAL_URL).getImage();
	public static final Image RIGHT_BLUE_PORTAL = new ImageIcon(
			RIGHT_BLUE_PORTAL_URL).getImage();

	/* Labyrinth */
	public static final String MAP_URL = "img/map/labyrinth.png";
	public static final Image MAP = new ImageIcon(MAP_URL).getImage();

	/* Background */
	public static final String BG_URL = "img/backgrounds/background.png";
	public static final Image BG = new ImageIcon(BG_URL).getImage();

	/* Animations */
	public static final String START_ANIM_URL = "img/backgrounds/start.gif";
	public static final Image START_ANIM = new ImageIcon(START_ANIM_URL)
			.getImage();

	// ========== </Contants> ==========
}
