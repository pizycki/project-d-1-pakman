package pakman.GUI;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import pakman.contracts.database.Database;
import pakman.contracts.database.RecordI;

@SuppressWarnings("serial")
public class HighscorePanel extends JPanel implements ActionListener {

	// ========== <Methods> ==========
	@Override
	public void paintComponent(Graphics g) {
		Graphics2D g2d = (Graphics2D) g;
		g2d.drawImage(background, -5, 5, this);
	}

	@Override
	public void actionPerformed(ActionEvent evt) {
		Object source = evt.getSource();
		if (source == _btnBack) {
			container.remove(this);
			container.add(new MenuPanel(this.container, dbc));
			container.validate();
			container.repaint();
		}
	}

	// ========== </Methods> ==========

	// ========== <Constructors> ==========
	public HighscorePanel(GameFrame container, Database db) {
		this.container = container;
		this.dbc = db;
		background = ImagesRepository.BG;
		try {
			dbc.createRecordList();
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}

		List<RecordI> _highscoreList = dbc.getList();
		Object[][] data = new Object[_highscoreList.size()][_highscoreList
				.size()];
		String[] columns = { NICK_COLUMN_LABEL, SCORE_COLUMN_LABEL };
		for (int i = 0; i < dbc.getList().size(); i++) {
			data[i] = _highscoreList.get(i).toTable();
		}
		dbc.clearRecordList();

		setLayout(null);

		_tbnModel = new TableModel(data, columns);
		_tblHighscore = new JTable(_tbnModel);
		_scrPane = new JScrollPane(_tblHighscore);
		_scrPane.setBounds(68, 25, 350, 350);
		add(_scrPane);

		_btnBack.setBounds(167, 400, 150, 30);
		_btnBack.setText(BACK_BTN_LABEL);
		_btnBack.addActionListener(this);
		add(_btnBack);
	}

	// ========== </Constructors> ==========

	// ========== <Members> ==========
	private Image background;
	private JButton _btnBack = new JButton();
	private TableModel _tbnModel;
	private JTable _tblHighscore;
	private JScrollPane _scrPane;
	private GameFrame container;
	private Database dbc;

	// ========== </Members> ==========

	// ========== <Constants> ==========
	public static final String NICK_COLUMN_LABEL = "NICK";
	public static final String SCORE_COLUMN_LABEL = "SCORE";
	public static final String BACK_BTN_LABEL = "BACK TO MENU";

	// ========== </Constants> ==========

}
