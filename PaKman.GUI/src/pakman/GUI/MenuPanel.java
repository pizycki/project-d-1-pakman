package pakman.GUI;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JPanel;

import pakman.contracts.database.Database;

@SuppressWarnings("serial")
public class MenuPanel extends JPanel implements ActionListener {

	// ========== <Members> ==========
	private BufferedImage image;
	private JButton _btnNewGame = new JButton();
	private JButton _btnMulti = new JButton();
	private JButton _btnJoin = new JButton();
	private JButton _btnHighscore = new JButton();
	private JButton _btnExit = new JButton();
	private GameFrame gameFrame;
	private Database db;

	// ========== </Members> ==========

	// ========== <Constructors> ==========
	public MenuPanel(GameFrame gameFrame, Database db) {
		this.gameFrame = gameFrame;
		this.db = db;

		File imageFile = new File("img/backgrounds/background.png");
		try {
			image = ImageIO.read(imageFile);
		} catch (IOException e) {
			System.err.println("Blad odczytu obrazka");
			e.printStackTrace();
		}

		setLayout(null);

		_btnNewGame.setBounds(167, 100, 150, 30);
		_btnNewGame.setText("NEW GAME");
		_btnNewGame.addActionListener(this);

		_btnMulti.setBounds(167, 160, 150, 30);
		_btnMulti.setText("MULTIPLAYER");
		_btnMulti.addActionListener(this);

		_btnJoin.setBounds(167, 220, 150, 30);
		_btnJoin.setText("DO��CZ DO GRY");
		_btnJoin.addActionListener(this);

		_btnHighscore.setBounds(167, 280, 150, 30);
		_btnHighscore.setText("HIGHSCORE");
		_btnHighscore.addActionListener(this);

		_btnExit.setBounds(167, 340, 150, 30);
		_btnExit.setText("EXIT");
		_btnExit.addActionListener(this);

		add(_btnNewGame);
		add(_btnMulti);
		add(_btnJoin);
		add(_btnHighscore);
		add(_btnExit);

	}

	// ========== </Constructors> ==========

	// ========== <Getters / Setters> ==========

	// ========== </Getters / Setters> ==========

	// ========== <Methods> ==========
	@Override
	public void actionPerformed(ActionEvent evt) {
		Object source = evt.getSource();
		// GAME INSTANCE
		if (source == _btnNewGame) {
			this.gameFrame.remove(this);

			this.gameFrame.getGuiController().newGameSelected();

		}
		// MULTI GAME INSTANCE
		else if (source == _btnMulti) {
			System.out.println("nowa instancja multi gry");

			this.gameFrame.remove(this);
			this.gameFrame.add(new MultiGamePanel(this.gameFrame, db));
			this.gameFrame.validate();
			this.gameFrame.repaint();

		}
		// DO��CZENIE DO GRY
		else if (source == _btnJoin) {
			System.out.println("do��czenie do multi gry");

			this.gameFrame.remove(this);
			this.gameFrame.add(new JoinPanel(gameFrame, db));
			this.gameFrame.validate();
			this.gameFrame.repaint();

		}
		// HIGHSCORE
		else if (source == _btnHighscore) {
			this.gameFrame.remove(this);
			this.gameFrame.add(new HighscorePanel(gameFrame, db));
			this.gameFrame.validate();
			this.gameFrame.repaint();
		}
		// EXIT
		else if (source == _btnExit) {
			System.exit(0);
		}
	}

	@Override
	public void paintComponent(Graphics g) {
		Graphics2D g2d = (Graphics2D) g;
		g2d.drawImage(image, -5, 5, this);
	}

	// ========== </Methods> ==========

}
