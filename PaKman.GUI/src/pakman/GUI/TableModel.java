package pakman.GUI;

import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

@SuppressWarnings("serial")
public class TableModel extends AbstractTableModel {

	// ========== <Members> ==========
	private ArrayList<Object[]> _list;
	private String[] _header;

	// ========== </Members> ==========

	// ========== <Constructors> ==========
	public TableModel(Object[][] obj, String[] header) {
		this._header = header;
		_list = new ArrayList<Object[]>();
		for (int i = 0; i < obj.length; ++i) {
			_list.add(obj[i]);
		}
	}

	// ========== </Constructors> ==========

	// ========== <Getters / Setters> ==========
	@Override
	public int getColumnCount() {
		return _header.length;
	}

	@Override
	public int getRowCount() {
		return _list.size();
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		return _list.get(rowIndex)[columnIndex];
	}

	public String getColumnName(int index) {
		return _header[index];
	}

	// ========== </Getters / Setters> ==========

	// ========== <Methods> ==========
	void add(String nick, String score) {
		String[] str = new String[2];
		str[0] = nick;
		str[1] = score;
		_list.add(str);
		fireTableDataChanged();
	}
	// ========== </Methods> ==========
}
