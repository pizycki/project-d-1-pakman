package pakman.GUI;

import java.awt.Color;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

import pakman.contracts.database.Database;

@SuppressWarnings("serial")
public class StartingPanel extends JPanel {

	// ========== <Members> ==========
	private JLabel gif;

	// ========== </Members> ==========

	// ========== <Constructors> ==========
	public StartingPanel(Database database) {
		setBackground(Color.BLACK);
		gif = new JLabel();
		gif.setIcon(new ImageIcon(ImagesRepository.START_ANIM_URL));
		add(gif);
	}
	// ========== </Constructors> ==========
}
