package pakman.GUI;

import java.awt.BorderLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;

import pakman.contracts.gamestate.GameState;

//import pakman.gamestate.contract.GameState;

@SuppressWarnings("serial")
public class TopGamePanel extends JPanel {

	// ========== <Members> ==========
	private GameState gameState;
	private JLabel scoreLabel;

	// ========== </Members> ==========

	// ========== <Constructors> ==========
	public TopGamePanel(GameState gameState) {

		this.gameState = gameState;

		// Prepare labels
		scoreLabel = new JLabel("0");
		this.add(scoreLabel, BorderLayout.CENTER);
	}

	// ========== </Constructors> ==========

	// ========== <Methods> ==========
	public void updateTotalScore() {
		// TODO change
		this.scoreLabel.setText("Total score = "
				+ this.gameState.getTotalScore() + "; Paku X: "
				+ this.gameState.getHero1().getLocX() + "; Y: "
				+ this.gameState.getHero1().getLocY());
	}
	// ========== </Methods> ==========
}
