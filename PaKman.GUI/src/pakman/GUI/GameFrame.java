package pakman.GUI;

import java.awt.Color;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JFrame;
import javax.swing.JPanel;

import pakman.common.Direction;
import pakman.contracts.database.Database;
import pakman.contracts.gamestate.GameState;
import pakman.contracts.gui.GuiException;
import pakman.contracts.sound.SoundController;
import pakman.contracts.sound.SoundFactory;

@SuppressWarnings("serial")
public class GameFrame extends JFrame {

	// ========== <Methods> ==========
	// Add GameWindow with new game instance
	public void loadNewGameWindow(GameState gameState) {
		try {
			// Remove all
			this.menuPanel = null;
			this.gameWindow = new GameWindow(gameState);
			this.add(gameWindow);
			this.gameWindow.setVisible(true);
			this.validate();
			this.repaint();

			// Swap keyListener
			this.removeKeyListener(menuKeyListener);
			this.addKeyListener(gameKeyListener);

			this.requestFocus();

		} catch (GuiException e) {
			e.printStackTrace();
			System.out.println("Forget about creating new game?");
		}
	}

	public void repaintGameWindow() {
		if (this.gameWindow != null)
			gameWindow.repaint();
	}

	public void switchToMenu() {
		// Remove windows
		if (startingPanel != null) {
			remove(startingPanel);
			startingPanel = null;
		}
		if (gameWindow != null) {
			remove(gameWindow);
			gameWindow = null;
		}

		// Add window
		if (menuPanel == null)
			menuPanel = new MenuPanel(this, database);
		add(menuPanel);
		validate();
		repaint();
	}

	// ========== </Methods> ==========

	// ========== <Getters / Setters> ==========

	public GuiController getGuiController() {
		return this.controller;
	}

	// ========== </Getters / Setters> ==========

	// ========== <Constructors> ==========
	public GameFrame(GuiController controller, String frameTitle,
			Database database) {

		this.controller = controller;
		this.database = database;

		this.startingPanel = new StartingPanel(database);

		setTitle(frameTitle);
		setBackground(Color.BLACK);
		setResizable(true);
		setLocation(400, 100);
		add(startingPanel);
		// Set menu key listener for keyboard listening
		addKeyListener(menuKeyListener);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		pack();
		setSize(490, 514);
		requestFocus();
	}

	// ========== </Constructors> ==========

	// ========== <Members> ==========
	private JPanel startingPanel;
	private JPanel menuPanel;
	private GuiController controller;
	private GameWindow gameWindow;
	private Database database;

	/**
	 * Used in game.
	 */
	KeyListener gameKeyListener = new KeyListener() {

		@Override
		public void keyTyped(KeyEvent arg0) {
			// do nothing
		}

		@Override
		public void keyReleased(KeyEvent arg0) {
			int keyCode = arg0.getKeyCode();
			Direction direction = Direction.Undifined;
			if (keyCode == KeyEvent.VK_UP)
				direction = Direction.Up;
			else if (keyCode == KeyEvent.VK_DOWN)
				direction = Direction.Down;
			else if (keyCode == KeyEvent.VK_LEFT)
				direction = Direction.Left;
			else if (keyCode == KeyEvent.VK_RIGHT)
				direction = Direction.Right;

			if (direction != Direction.Undifined)
				controller.changePlayerDirection(direction);
		}

		@Override
		public void keyPressed(KeyEvent arg0) {

			System.out.println(arg0.getKeyCode());
			// do nothing
		}
	};

	/*
	 * Used in menu.
	 */
	KeyListener menuKeyListener = new KeyListener() {
		public void keyPressed(KeyEvent e) {
			switchToMenu();
		}

		@Override
		public void keyReleased(KeyEvent e) {
			// do nothing

		}

		@Override
		public void keyTyped(KeyEvent e) {
			// do nothing
		}
	};
	// ========== </Members> ==========

}
