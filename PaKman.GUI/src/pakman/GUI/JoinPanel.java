package pakman.GUI;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;

import pakman.contracts.database.Database;

@SuppressWarnings("serial")
public class JoinPanel extends JPanel implements ActionListener {

	// ========== <Members> ==========
	private Image image;
	private JButton btnBack = new JButton();
	private GameFrame container;
	private Database db;

	// ========== </Members> ==========

	// ========== <Constructors> ==========
	public JoinPanel(GameFrame container, Database db) {
		this.container = container;
		image = ImagesRepository.BG;
		btnBack.setBounds(167, 400, 150, 30);
		btnBack.setText("BACK TO MENU");
		btnBack.addActionListener(this);

		add(btnBack);
	}

	// ========== </Constructors> ==========

	// ========== <Methods> ==========
	@Override
	public void paintComponent(Graphics g) {
		Graphics2D g2d = (Graphics2D) g;
		g2d.drawImage(image, -5, 5, this);
	}

	@Override
	public void actionPerformed(ActionEvent evt) {
		Object source = evt.getSource();
		if (source == btnBack) {
			this.container.remove(this);
			this.container.remove(this);
			this.container.add(new MenuPanel(this.container, db));
			this.container.validate();
			this.container.repaint();
		}
	}
	// ========== </Methods> ==========
}
