package pakman.run;

import pakman.GUI.GuiController;
import pakman.characters.impl.CharactersFactory;
import pakman.client.impl.GameClient;
import pakman.common.Direction;
import pakman.consumable.impl.ConsumablesFactory;
import pakman.contracts.characters.CharacterFactory;
import pakman.contracts.characters.Enemy;
import pakman.contracts.characters.Hero;
import pakman.contracts.consumables.ConsumbleFactory;
import pakman.contracts.database.Database;
import pakman.contracts.gamestate.GameState;
import pakman.contracts.gamestate.GameStateFactory;
import pakman.contracts.gui.Gui;
import pakman.contracts.host.Host;
import pakman.contracts.labyrinth.Map;
import pakman.contracts.labyrinth.MapFactory;
import pakman.contracts.logger.Logger;
import pakman.contracts.messages.JsonConverter;
import pakman.contracts.messages.MessageFactory;
import pakman.contracts.sound.SoundController;
import pakman.contracts.sound.SoundFactory;
import pakman.contracts.udp.UdpClient;
import pakman.contracts.udp.UdpFactory;
import pakman.contracts.udp.UdpServer;
import pakman.database.impl.DatabaseCore;
import pakman.gamestate.impl.GameStateFactoryImpl;
import pakman.host.impl.GameHost;
import pakman.labyrinth.impl.LabirynthFactory;
import pakman.logger.impl.FileLogger;
import pakman.messages.impl.GsonConverter;
import pakman.messages.impl.MessageFactoryImpl;
import pakman.sound.impl.SoundControllerImpl;
import pakman.sound.impl.SoundFactoryImpl;
import pakman.udp.impl.UdpFactoryImpl;
import pk.glk.soundplayer.ISound;
import pk.glk.soundplayer.internal.SoundPlayer;

public class GameMainProgram {

	/**
	 * Program starts here.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		new GameMainProgram();
	}

	/**
	 * Prepares game program to run.
	 */
	public GameMainProgram() {
		try {
			int cellWidth = 25;
			int cellHeight = 25;
			int port = 5678;
			String address = "localhost";
			int timeout = 1000;

			Logger logger = new FileLogger();
			CharacterFactory cf = new CharactersFactory(cellWidth, cellHeight);
			GameStateFactory gsf = new GameStateFactoryImpl();
			ConsumbleFactory itemsFactory = new ConsumablesFactory();
			MapFactory mf = new LabirynthFactory(cellWidth, cellHeight,
					itemsFactory);
			UdpFactory udpf = new UdpFactoryImpl();
			UdpClient udpClient = udpf.createClient(port, address);
			UdpServer udpSrv = udpf.createServer(port, timeout);

			// TODO Usu� game state kiedy client b�dzie mniej zale�ny od hosta.
			GameState gs = createGamestate(cf, gsf, mf);

			MessageFactory msgFac = new MessageFactoryImpl();
			JsonConverter jsonConv = new GsonConverter();

			@SuppressWarnings("unused")
			Host host = new GameHost(mf, cf, gsf, udpSrv, logger, msgFac,
					jsonConv);

			ISound soundPlayer = new SoundPlayer();
			SoundFactory sf = new SoundFactoryImpl();
			SoundController sc = new SoundControllerImpl(soundPlayer);

			Database db = DatabaseCore.getInstace();
			Gui gui = new GuiController("Pakman!", sc, sf, db);

			GameClient client = new GameClient(gui, udpClient, gs, sc, sf, db,
					msgFac, jsonConv);

			gui.addNewGameSelectedEventListener(client);
			gui.addPlayerDirectionSelectEventListener(client);

			gui.show();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// TODO remove
	public GameState createGamestate(CharacterFactory cf, GameStateFactory gsf,
			MapFactory mf) {
		int stepLength = 1;
		Map labyrinth = mf.create();
		// Create hero 1
		Hero hero1 = cf.createHero("Paku", 1, Direction.Left, stepLength, 225,
				350);
		// Create hero 2
		Hero hero2 = cf.createHero("Quaqu", 1, Direction.Left, stepLength, 250,
				350);
		// Create four enemies
		Enemy e1 = cf.createEnemy("Pikej", Direction.Right, stepLength, 200,
				200);
		Enemy e2 = cf.createEnemy("Homer", Direction.Up, stepLength, 225, 200);
		Enemy e3 = cf.createEnemy("Hannibal", Direction.Up, stepLength, 225,
				200);
		Enemy e4 = cf
				.createEnemy("Rasta", Direction.Left, stepLength, 250, 200);
		return gsf.createNewSession(hero1, hero2, e1, e2, e3, e4, labyrinth);
	}
}
