package pakman.controller.impl;

import java.util.ArrayList;
import java.util.List;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import pakman.contracts.controller.Controller;
import pakman.contracts.controller.ControllerListener;

public class KeyboardController extends KeyAdapter implements Controller {

	// ========== <Methods> ==========

	public void RiseKeyPressed(int key) {
		for (int i = 0; i < listeners.size(); ++i)
			listeners.get(i).onKeyPressed(key);

	}

	// ========== </Methods> ==========

	@Override
	public void keyPressed(KeyEvent e) {
		int keyCode = e.getKeyCode();

		if ((e.getKeyCode() == KeyEvent.VK_UP)
				|| (e.getKeyCode() == KeyEvent.VK_DOWN)
				|| (e.getKeyCode() == KeyEvent.VK_LEFT)
				|| (e.getKeyCode() == KeyEvent.VK_RIGHT)
				|| (e.getKeyCode() == KeyEvent.VK_0)
				|| (e.getKeyCode() == KeyEvent.VK_1)
				|| (e.getKeyCode() == KeyEvent.VK_2)
				|| (e.getKeyCode() == KeyEvent.VK_3)
				|| (e.getKeyCode() == KeyEvent.VK_4)
				|| (e.getKeyCode() == KeyEvent.VK_5)
				|| (e.getKeyCode() == KeyEvent.VK_6)
				|| (e.getKeyCode() == KeyEvent.VK_7)
				|| (e.getKeyCode() == KeyEvent.VK_8)
				|| (e.getKeyCode() == KeyEvent.VK_9)
				|| (e.getKeyCode() == KeyEvent.VK_A)
				|| (e.getKeyCode() == KeyEvent.VK_B)
				|| (e.getKeyCode() == KeyEvent.VK_C)
				|| (e.getKeyCode() == KeyEvent.VK_D)
				|| (e.getKeyCode() == KeyEvent.VK_E)
				|| (e.getKeyCode() == KeyEvent.VK_F)
				|| (e.getKeyCode() == KeyEvent.VK_G)
				|| (e.getKeyCode() == KeyEvent.VK_H)
				|| (e.getKeyCode() == KeyEvent.VK_I)
				|| (e.getKeyCode() == KeyEvent.VK_J)
				|| (e.getKeyCode() == KeyEvent.VK_K)
				|| (e.getKeyCode() == KeyEvent.VK_L)
				|| (e.getKeyCode() == KeyEvent.VK_M)
				|| (e.getKeyCode() == KeyEvent.VK_N)
				|| (e.getKeyCode() == KeyEvent.VK_O)
				|| (e.getKeyCode() == KeyEvent.VK_P)
				|| (e.getKeyCode() == KeyEvent.VK_Q)
				|| (e.getKeyCode() == KeyEvent.VK_R)
				|| (e.getKeyCode() == KeyEvent.VK_S)
				|| (e.getKeyCode() == KeyEvent.VK_T)
				|| (e.getKeyCode() == KeyEvent.VK_U)
				|| (e.getKeyCode() == KeyEvent.VK_V)
				|| (e.getKeyCode() == KeyEvent.VK_W)
				|| (e.getKeyCode() == KeyEvent.VK_X)
				|| (e.getKeyCode() == KeyEvent.VK_Y)
				|| (e.getKeyCode() == KeyEvent.VK_Z)) {
			RiseKeyPressed(keyCode);
		}
		e.consume();
	}

	// ========== <Events> ==========

	protected List<ControllerListener> listeners = new ArrayList<ControllerListener>();

	@Override
	public void addControllerListener(ControllerListener listener) {
		listeners.add(listener);
	}

	@Override
	public void removeControllerListener(ControllerListener listener) {
		listeners.remove(listener);
	}
	// ========== </Events> ==========

}