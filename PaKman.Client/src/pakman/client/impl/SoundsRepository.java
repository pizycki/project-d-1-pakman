package pakman.client.impl;

import pakman.contracts.sound.Sound;
import pakman.contracts.sound.SoundFactory;

/**
 * This class is repository of sounds used by game client. It is variation of
 * singleton. To create a new instance of this class, you need to provide
 * SoundFactory implementation first.
 * 
 * @author Magda
 * 
 */
public class SoundsRepository {

	// ========== <Getters / Setters> ==========

	/**
	 * Returns instance of sounds repository.
	 * 
	 * @return Instance.
	 */
	public static SoundsRepository getInstance() {
		if (instance == null && factory != null) {
			instance = new SoundsRepository();
		}

		return instance;
	}

	/**
	 * Sets factory implementation on static field and return instance of sounds
	 * repository.
	 * 
	 * @param "SoundFactory implementation."
	 * @return Instance.
	 */
	public static SoundsRepository getInstance(SoundFactory factory) {
		if (factory != null) {
			SoundsRepository.setSoundFactory(factory);
		}

		return getInstance();
	}

	/**
	 * Sets factory implementation on static field.
	 * 
	 * @param "SoundFactory implementation."
	 */
	public static void setSoundFactory(SoundFactory factory) {
		SoundsRepository.factory = factory;
	}

	public Sound getWakaSound() {
		return wakaSound;
	}

	public Sound getSirenSound() {
		return sirenSound;
	}
	
	public Sound getOpenSound() {
		return openSound;
	}
	
	public Sound getMenuSound() {
		return menuSound;
	}
	
	public Sound getDeathSound() {
		return deathSound;
	}
	
	public Sound getHighscoreSound() {
		return highscoreSound;
	}
	

	// ========== </Getters / Setters> ==========

	// ========== <Constructors> ==========
	/**
	 * Constructor. Creates sounds using provided factory implementation.
	 */
	private SoundsRepository() {
		// Factory implementation should be already set.
		this.sirenSound = factory.create("Siren", "sounds/Pacman_Siren.wav",
				450);
		this.wakaSound = factory.create("Waka-waka", "sounds/Pacman_Waka_Waka.wav",
				600);
		this.openSound = factory.create("OpenSong", "sounds/Pacman_Opening_Song.wav",
				100000);
		this.menuSound = factory.create("MenuSong", "sounds/Pacman_Menu_Song.wav",
				0);
		this.deathSound = factory.create("Death", "sounds/Pacman_Death.wav",
				600);
		this.highscoreSound = factory.create("Highscore", "sounds/Pacman_Highscore.wav",
				600);
	}

	// ========== </Constructors> ==========

	// ========== <Members> ==========
	protected final Sound wakaSound;
	protected final Sound sirenSound;
	protected final Sound openSound;
	protected final Sound menuSound;
	protected final Sound deathSound;
	protected final Sound highscoreSound;
	private static SoundsRepository instance;
	private static SoundFactory factory;
	// ========== </Members> ==========
}
