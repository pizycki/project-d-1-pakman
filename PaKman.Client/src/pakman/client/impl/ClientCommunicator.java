package pakman.client.impl;

import java.io.IOException;

import pakman.common.Direction;
import pakman.contracts.characters.Enemy;
import pakman.contracts.client.ClientException;
import pakman.contracts.client.Communicator;
import pakman.contracts.gamestate.GameState;
import pakman.contracts.gamestate.GameType;
import pakman.contracts.labyrinth.ContainingField;
import pakman.contracts.labyrinth.Field;
import pakman.contracts.messages.ClientCreateNewSession;
import pakman.contracts.messages.ClientGameUpdateMessage;
import pakman.contracts.messages.HostGameUpdateMessage;
import pakman.contracts.messages.HostSessionCreated;
import pakman.contracts.messages.HostWaitingForPlayers;
import pakman.contracts.messages.JsonConverter;
import pakman.contracts.messages.Message;
import pakman.contracts.messages.MessageFactory;
import pakman.contracts.messages.MessageType;
import pakman.contracts.udp.UdpClient;
import pakman.contracts.udp.UdpException;

/**
 * This class is a proxy for Host.
 * 
 * @author Pawe�
 * 
 */
public class ClientCommunicator implements Communicator {

	// ========== <Methods> ==========

	/**
	 * Send data to host and awaits for response
	 */
	public void syncWithHost() {

		// Prepare message for host
		ClientGameUpdateMessage clientMsg = getClientMessage();

		// System.out.println("ClientMessage przed wys�aniem: "
		// + clientMsg.toString());
		String json = jsonConverter.convertTo(clientMsg);

		String rawResponse = new String();
		try {
			rawResponse = this.udpClnt.sendAndReceive(json);
		} catch (UdpException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		// Get type of the message
		Class<?> clazz = jsonConverter.getMessageType(rawResponse);
		Message response = (Message) jsonConverter.convertFrom(clazz,
				rawResponse);

		// When received response is type of Host Game Update Message
		if (response instanceof HostGameUpdateMessage) {
			HostGameUpdateMessage hostMsg = (HostGameUpdateMessage) response;

			// System.out.println("HostMessage po odebraniu: "
			// + hostMsg.toString());

			updateLocalGameState(hostMsg);
		}

	}

	/**
	 * Updates local instance of game state
	 * 
	 * @param message
	 */
	protected void updateLocalGameState(HostGameUpdateMessage message) {
		GameState gs = this.gameState;

		if (message.isGameOver()) {
			// stop updating
		}

		// Hero
		gs.getHero1().setLocX(message.getH1X());
		gs.getHero1().setLocY(message.getH1Y());
		gs.getHero1().setDirection(message.getH1dir());

		// Enemies locations
		Enemy[] enemies = gs.getEnemies().toArray(
				new Enemy[gs.getEnemies().size()]);

		// Enemy 1
		enemies[0].setLocX(message.getE1X());
		enemies[0].setLocY(message.getE1Y());
		enemies[0].setDirection(message.getE1dir());
		// Enemy 2
		enemies[1].setLocX(message.getE2X());
		enemies[1].setLocY(message.getE2Y());
		enemies[1].setDirection(message.getE2dir());
		// Enemy 3
		enemies[2].setLocX(message.getE3X());
		enemies[2].setLocY(message.getE3Y());
		enemies[2].setDirection(message.getE3dir());
		// Enemy 4
		enemies[3].setLocX(message.getE4X());
		enemies[3].setLocY(message.getE4Y());
		enemies[3].setDirection(message.getE4dir());

		// Consuming items
		if (message.isConsumedItem()) {
			int col = gs.getMap().getFieldsColumn(message.getItemX());
			int row = gs.getMap().getFieldsRow(message.getItemY());

			Field field = gs.getMapStateField(row, col);
			if (field instanceof ContainingField) {
				ContainingField cField = (ContainingField) field;
				if (cField.isContainingItem()) {
					cField.removeItem();
				}
			}
		}

		// Player score
		gameState.setPlayerScore(message.getPlayerScore());

		// Remaining lives
		gameState.getHero1().setLives((byte) message.getLivesLeft());

		// Hero killed
		gameState.setHeroKilled(message.isHeroKilled());

		// Game over
		gameState.setGameOver(message.isGameOver());

		// Game started
		gameState.setGameStarted(message.isGameStarted());

		// Game type
		gameState.setGameType(message.getGameType());

		// Game prepared
		gameState.setGamePrepared(message.isGamePrepared());

	}

	/**
	 * 
	 * @param gameType
	 * @throws ClientException
	 */
	public boolean createNewSession(GameType gameType) {
		// Prepare message for host
		ClientCreateNewSession message = (ClientCreateNewSession) messageFactory
				.create(MessageType.ClientCreateNewSession);
		message.setGameType(gameType);
		String json = jsonConverter.convertTo(message);

		// Send msg and receive response
		String rawResponse = "";
		try {
			rawResponse = udpClnt.sendAndReceive(json);
		} catch (UdpException | IOException e) {
			e.printStackTrace();
		}

		// Get type of the message
		Class<?> clazz = jsonConverter.getMessageType(rawResponse);
		Message response = (Message) jsonConverter.convertFrom(clazz,
				rawResponse);

		// For singleplayer
		if (gameType == GameType.Singleplayer) {
			if (response instanceof HostSessionCreated) {
				HostSessionCreated hsc = (HostSessionCreated) response;
				return true;
			} else {
				return false;

				// throw new GameException(
				// "Expecting HostSessionCreated message.");
			}
		}

		// For multiplayer
		if (gameType == GameType.Multiplayer) {
			if (response instanceof HostWaitingForPlayers) {
				HostWaitingForPlayers hwfp = (HostWaitingForPlayers) response;

				//
				return true;

			} else {
				return false;
				// throw new GameException(
				// "Expecting HostWaitingForPlayers message.");
			}
		}

		return false;
	}

	// ========== </Methods> ==========

	// ========== <Getters / Setters> ==========

	/**
	 * Actual client message to be sent to host. If instance is not set, creates
	 * new blank one
	 * 
	 * @return Actual client message.
	 */
	public ClientGameUpdateMessage getClientMessage() {

		ClientGameUpdateMessage message;
		if (this.clntMessage == null)
			message = (ClientGameUpdateMessage) this.messageFactory
					.create(MessageType.ClientGameUpdate);
		else
			message = this.clntMessage;

		return message;
	}

	/**
	 * Returns local game state
	 */
	public GameState getGameState() {
		return this.gameState;
	}

	/**
	 * Sets reference to gamestate.
	 * 
	 * @param ref
	 */
	public void setGameState(GameState gamestate) {
		this.gameState = gamestate;
	}

	/**
	 * Sets players direction.
	 */
	@Override
	public void setPlayerDirection(Direction direction) {
		if (clntMessage != null && direction != Direction.Undifined) {
			clntMessage.setPlayerDirection(direction);
		}
	}

	// ========== </Getters / Setters> ==========

	// ========== <Constructors> ==========
	/**
	 * Constructor
	 * 
	 * @param gameState
	 * @param udpClnt
	 * @throws ClientException
	 */
	public ClientCommunicator(GameState gameState, UdpClient udpClnt,
			MessageFactory msgFactory, JsonConverter jsonConv)
			throws ClientException {

		// UDP Client
		if (udpClnt == null)
			throw new ClientException("UdpClient is null!");
		this.udpClnt = udpClnt;

		// GameState
		if (gameState == null)
			throw new ClientException("Game state is null!");
		this.gameState = gameState;

		// MessageFactory
		if (msgFactory == null)
			throw new ClientException("Message factory is null!");
		this.messageFactory = msgFactory;

		// Json Converter
		if (jsonConv == null)
			throw new ClientException("Json Converter is null!");
		this.jsonConverter = jsonConv;

		// Prepare blank client message
		this.clntMessage = (ClientGameUpdateMessage) messageFactory
				.create(MessageType.ClientGameUpdate);
	}

	// ========== </Constructors> ==========

	// ========== <Members> ==========

	protected ClientGameUpdateMessage clntMessage;
	protected GameState gameState;
	protected MessageFactory messageFactory;
	protected JsonConverter jsonConverter;
	private UdpClient udpClnt;

	// ========== </Members> ==========

}
