package pakman.client.impl;

import java.util.HashMap;
import java.util.Map;

/**
 * This is configuration class.
 * 
 * @author Pawe�
 * 
 */
public class Configuration {

	// ========== <Singleton> ==========
	public static Configuration getInstance() {
		if (instance == null) {
			instance = new Configuration();
		}
		return instance;
	}

	private Configuration() {
		this.configs = new HashMap<String, Object>();
		this.configs.put(DRAW_ENTERABLE_FIELD_AVAIBLE_EXITS, false);
		this.configs.put(DRAW_GAME_WINDOW_RED_RECTANGLE, false);
		this.configs.put(DRAW_OBSTACLES, false);
		this.configs.put(SOUND_ENABLED, true);
		this.configs.put(CLIENT_REFRESH_INTERVAL, 10);
		this.configs.put(TITLE, "PaKman!");
	}

	private static Configuration instance;

	// ========== </Singleton> ==========

	// ========== <Methods> ==========
	@SuppressWarnings("unchecked")
	public <T> T getValue(String key) {
		if (!this.configs.containsKey(key))
			return null;

		T val = null;
		try {
			val = (T) this.configs.get(key);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return val;
	}

	// ========== </Methods> ==========

	// ========== <Members> ==========
	private Map<String, Object> configs;
	// ========== </Members> ==========

	// ========== <Contants> ==========
	public static final String DRAW_ENTERABLE_FIELD_AVAIBLE_EXITS = "DRAW_ENTERABLE_FIELD_AVAIBLE_EXITS";
	public static final String DRAW_GAME_WINDOW_RED_RECTANGLE = "DRAW_GAME_WINDOW_RED_RECTANGLE";
	public static final String DRAW_OBSTACLES = "DRAW_OBSTACLES";
	public static final String SOUND_ENABLED = "SOUND_ENABLED";
	public static final String CLIENT_REFRESH_INTERVAL = "CLIENT_REFRESH_INTERVAL";
	public static final String TITLE = "TITLE";
	// ========== </Contants> ==========

}
