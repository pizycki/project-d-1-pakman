package pakman.client.impl;

import java.util.Timer;
import java.util.TimerTask;

import pakman.common.Direction;
import pakman.contracts.client.Client;
import pakman.contracts.client.ClientException;
import pakman.contracts.client.Communicator;
import pakman.contracts.database.Database;
import pakman.contracts.gamestate.GameState;
import pakman.contracts.gamestate.GameType;
import pakman.contracts.gui.Gui;
import pakman.contracts.messages.JsonConverter;
import pakman.contracts.messages.MessageFactory;
import pakman.contracts.sound.SoundController;
import pakman.contracts.sound.SoundFactory;
import pakman.contracts.udp.UdpClient;

public class GameClient implements Client {

	// ========== <Events> ==========

	@Override
	public void onNewGameSelectedEvent() {
		try {
			// // if current game state is changed since init., restore
			if (gameStateVanilla != null && this.gameState.isGameOver())
				this.gameState = gameStateVanilla.getDeepCopy();
			host.setGameState(this.gameState);
			this.graphicInterface.prepareNewGame(this.gameState);
			createNewRound();
		} catch (ClientException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onPlayerDirectionSelectEvent(Direction direction) {
		host.setPlayerDirection(direction);
	}

	// ========== </Events> ==========

	// ========== <Methods> ==========

	private void onTimerTick() {
		// Update game state
		host.syncWithHost();

		// Update view
		graphicInterface.updateGame();

		// Play sounds
		if (App.Config.<Boolean> getValue(Configuration.SOUND_ENABLED)) {
			this.soundController.playSound(App.Sounds.getOpenSound());

			if (this.gameState.isGameStarted() && !this.gameState.isGameOver()
					&& this.gameState.isGamePrepared()) {
				this.soundController.playSound(App.Sounds.getSirenSound());
			}
			if (this.gameState.isGameOver()) {
				this.soundController.playSound(App.Sounds.getDeathSound());
			}
		}

		// If game is over, stop syncing timer
		if (this.gameState.isGameOver()) {
			stopSyncingTimer();
			int score = this.gameState.getPlayerScore();
			this.database.makeRecord(score);
			this.graphicInterface.showMenuWindow();
		}
	}

	/**
	 * 
	 */
	public void startSyncingTimer() {
		mainTimer = new Timer();
		mainTimer.scheduleAtFixedRate(new TimerTask() {
			@Override
			public void run() {
				onTimerTick();
			}
		}, 0, mainTimerInterval);
	}

	/**
	 * 
	 */
	public void stopSyncingTimer() {
		if (this.mainTimer != null) {
			this.mainTimer.cancel();
			this.mainTimer = null;
		}
	}

	/**
	 * Crates new game
	 * 
	 * @throws ClientException
	 */
	public void createNewRound() throws ClientException {
		// Create new session
		boolean newSessionCreated = host
				.createNewSession(GameType.Singleplayer);

		if (newSessionCreated) {
			this.startSyncingTimer();
		} else
			throw new ClientException("Couldn't create new session.");
	}

	// ========== </Methods> ==========

	// ========== <Getters / Setters> ==========

	@Override
	public GameState getGameState() {
		return this.gameState;
	}

	@Override
	public String getTitle() {
		return App.Config.<String> getValue(Configuration.TITLE);
	}

	// ========== </Getters / Setters> ==========

	// ========== <Constructors> ==========
	public GameClient(Gui graphicInterface, UdpClient udpClnt,
			GameState gameState, SoundController soundController,
			SoundFactory soundsFactory, Database database,
			MessageFactory msgFactory, JsonConverter jsonConverter)
			throws ClientException {

		// Make a deep copy of game state for future recovering.
		this.gameStateVanilla = gameState.getDeepCopy();
		this.gameState = gameState.getDeepCopy();

		// Prepare database
		this.database = database;

		// Prepare GUI
		this.graphicInterface = graphicInterface;

		// Create host proxy
		host = new ClientCommunicator(gameState, udpClnt, msgFactory,
				jsonConverter);

		// // Prepare controller
		// this.controller = new KeyboardController();
		// this.controller.addControllerListener(host);

		// Prepare game state
		this.gameState = gameState;

		// Remember brand new gamestate

		// Set heros starting direction to left
		this.host.setPlayerDirection(Direction.Left);

		// Prepare SoundController
		this.soundController = soundController;

		// Set sounds factory in SoundsRepository
		SoundsRepository.setSoundFactory(soundsFactory);

		// Set client refresh interval
		mainTimerInterval = (long) App.Config
				.<Integer> getValue(Configuration.CLIENT_REFRESH_INTERVAL);
	}

	// ========== </Constructors> ==========

	// ========== <Members> ==========
	private Database database;
	private Gui graphicInterface;
	private GameState gameState;
	// private Controller controller;
	private Communicator host;
	private SoundController soundController;
	private Timer mainTimer;
	private GameState gameStateVanilla;
	private final long mainTimerInterval;
	// ========== </Members> ==========
	// ========== <Contants> ==========
	// ========== </Contants> ==========
}