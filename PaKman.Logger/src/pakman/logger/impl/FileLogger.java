package pakman.logger.impl;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import pakman.contracts.logger.Logger;

public class FileLogger implements Logger {

	// ========== <Members> ==========

	public static FileLogger instance = null;
	public static String openingMsg = "Komponent zosta� uruchomiony";
	public static String closingMsg = "Komponent zosta� zamkni�ty";

	// ========== </Members> ==========

	// ========== <Methods> ==========

	@Override
	public void WriteLine(String log) {
		WriteLine(log, "");
	}

	@Override
	public void WriteLine(String log, String invoker) {
		try (FileWriter out = new FileWriter("LOG.log", false)) {
			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat(
					"dd.MMM.YYYY HH:mm:ss:SSS ");
			BufferedWriter bw = new BufferedWriter(out);

			if (log.equals("")) {
				bw.append(" >>> " + invoker);
			}
			if (!log.equals("") && !invoker.equals("")) {
				bw.append(sdf.format(date) + log + ", ");
				bw.append(invoker);
			}
			if (!log.equals("") && invoker.equals("")) {
				bw.append("" + sdf.format(date) + log);
			}
			bw.newLine();
			bw.close();
			out.close();
		} catch (IOException e) {
			writeLine(e);
		}
	}

	@Override
	public void writeLine(Exception e) {
		WriteLine(e.getMessage(), e.getClass().getName());
		StackTraceElement[] tab = e.getStackTrace();
		for (int i = 0; i < tab.length; i++) {
			WriteLine("", tab[i].toString());
		}

	}

	public static FileLogger getInstace() {
		if (instance == null) {
			instance = new FileLogger();
		}
		return instance;
	}

	// ========== </Methods> ==========

}
