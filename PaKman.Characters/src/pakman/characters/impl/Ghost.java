package pakman.characters.impl;

import java.awt.Point;
import java.awt.geom.Point2D;

import pakman.common.Direction;
import pakman.contracts.characters.Enemy;

@SuppressWarnings("serial")
public class Ghost implements Enemy {

	// ========== <Methods> ==========
	/**
	 * Makes character to do a single step in specified direction.
	 */
	@Override
	public void makeStep() {
		switch (this.direction) {
		case Up:
			this.setLocY(this.locY - this.stepLength);
			break;
		case Right:
			this.setLocX(this.locX + this.stepLength);
			break;
		case Down:
			this.setLocY(this.locY + this.stepLength);
			break;
		case Left:
			this.setLocX(this.locX - this.stepLength);
			break;
		default:
			break;
		}

		// System.out.println(toString());
	}

	@Override
	public String toString() {
		return "Ghost [locX=" + locX + ", locY=" + locY + ", width=" + width
				+ ", height=" + height + ", name=" + name + ", direction="
				+ direction + ", stepLength=" + stepLength + "]";
	}

	// ========== </Methods> ==========

	// ========== <Getters / Setters> ==========

	public float getLocX() {
		return this.locX;
	}

	@Override
	public void setLocX(float x) {
		this.locX = x;
	}

	@Override
	public float getLocY() {
		return this.locY;
	}

	@Override
	public void setLocY(float y) {
		this.locY = y;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public Direction getDirection() {
		return direction;
	}

	@Override
	public void setDirection(Direction direction) {
		this.direction = direction;
	}

	@Override
	public float getStepLength() {
		return stepLength;
	}

	@Override
	public void setStepLength(float length) {
		this.stepLength = length;
	}

	@Override
	public Point getCenterPoint() {
		double midX = (double) (getLocX() + getWidth() / 2);
		double midY = (double) (getLocY() + getHeight() / 2);
		Point2D centerPoint = new Point();
		centerPoint.setLocation(midX, midY);
		return (Point) centerPoint;
	}

	@Override
	public int getWidth() {
		return (int) this.width;
	}

	@Override
	public int getHeight() {
		return (int) this.height;
	}
	
	
	// ========== <Constructors> ==========

	public Ghost(float locX, float locY, float width, float height,
			String name, Direction direction, float stepLength) {
		super();
		this.locX = locX;
		this.locY = locY;
		this.width = width;
		this.height = height;
		this.name = name;
		this.direction = direction;
		this.stepLength = stepLength;
	}

	// ========== </Constructors> ==========


	// ========== </Getters / Setters> ==========

	// ========== <Members> ==========
	private float locX;
	private float locY;
	private float width;
	private float height;
	private String name;
	private Direction direction = Direction.Undifined;
	private float stepLength;
	// ========== </Members> ==========

	// ========== <Contants> ==========

	// ========== </Contants> ==========

}
