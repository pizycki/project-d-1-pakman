package pakman.characters.impl;

import pakman.common.Direction;
import pakman.contracts.characters.CharacterFactory;
import pakman.contracts.characters.Enemy;
import pakman.contracts.characters.Hero;

/**
 * 
 * @author Pawe�
 * 
 */
public class CharactersFactory implements CharacterFactory {

	/**
	 * Creates an instance of Hero interface.
	 * 
	 * @param name
	 * @param lives
	 * @param direction
	 * @param stepLength
	 * @param locX
	 * @param locY
	 * @return
	 */
	@Override
	public Hero createHero(String name, int lives, Direction direction,
			int stepLength, int locX, int locY) {
		try {
			return new Pakman((float) locX, (float) locY,
					(float) this.charactersHeight,
					(float) this.charactersWidth, name, (byte) lives,
					direction, stepLength);
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * 
	 * @param name
	 * @param direction
	 * @param stepLength
	 * @param locX
	 * @param locY
	 * @return
	 */
	public Enemy createEnemy(String name, Direction direction, int stepLength,
			int locX, int locY) {
		try {
			return new Ghost((float) locX, (float) locY,
					(float) this.charactersWidth, (float) this.charactersWidth,
					name, direction, stepLength);
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * 
	 * @param charactersWidth
	 * @param charactersHeight
	 */
	public CharactersFactory(int charactersWidth, int charactersHeight) {
		this.charactersHeight = charactersHeight;
		this.charactersWidth = charactersWidth;
	}

	private int charactersWidth;
	private int charactersHeight;
}
