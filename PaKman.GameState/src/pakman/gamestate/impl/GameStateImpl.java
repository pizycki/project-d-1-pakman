package pakman.gamestate.impl;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import pakman.common.Direction;
import pakman.contracts.characters.Character;
import pakman.contracts.characters.Enemy;
import pakman.contracts.characters.Hero;
import pakman.contracts.gamestate.GameState;
import pakman.contracts.gamestate.GameType;
import pakman.contracts.labyrinth.Field;
import pakman.contracts.labyrinth.Map;

@SuppressWarnings("serial")
class GameStateImpl implements GameState {

	@Override
	public String toString() {
		return "GameStateImpl [hero1=" + hero1 + ", hero2=" + hero2
				+ ", enemies=" + enemies + ", map=" + map + ", playerScore="
				+ playerScore + ", isGameOver=" + isGameOver
				+ ", isHeroKilled=" + isHeroKilled + ", isGameStarted="
				+ isGameStarted + ", isGamePrepared=" + isGamePrepared
				+ ", gameType=" + gameType + ", charactersStartingProperties="
				+ Arrays.toString(charactersStartingProperties) + "]";
	}

	// ========== <Methods> ==========
	@Override
	public void resetCharacters() {
		if (this.charactersStartingProperties != null
				&& this.charactersStartingProperties.length != 0) {
			// Reset characters positions
			Character[] charactersToUpdate = new Character[SINGLEPLAYER_CHARACTERS_COUNT];
			charactersToUpdate[0] = this.hero1;
			int charsIdx = 1; // bc hero is already in array
			for (Enemy enm : this.enemies) {
				charactersToUpdate[charsIdx++] = enm;
			}
			for (int charIdx = 0; charIdx < this.charactersStartingProperties.length; charIdx++) {
				if (this.charactersStartingProperties[charIdx][0] instanceof Character) {
					for (Character character : charactersToUpdate) {
						if (character
								.equals(this.charactersStartingProperties[charIdx][0])) {
							character
									.setLocX((float) this.charactersStartingProperties[charIdx][1]);
							character
									.setLocY((float) this.charactersStartingProperties[charIdx][2]);
							character
									.setDirection(Direction
											.valueOf((String) this.charactersStartingProperties[charIdx][3]));
							break;
						}
					}
				}
			}
		}
	}

	// ========== </Methods> ==========

	// ========== <Getters / Setters> ==========
	@Override
	public Hero getHero1() {
		return this.hero1;
	}

	@Override
	public Hero getHero2() {
		return this.hero2;
	}

	@Override
	public Set<Enemy> getEnemies() {
		return this.enemies;
	}

	@Override
	public Map getMap() {
		return this.map;
	}

	public void setMap(Map map) {
		this.map = map;
	}

	@Override
	public int getTotalScore() {
		return this.playerScore;
	}

	@Override
	public void setPlayerScore(int score) {
		this.playerScore = score;
	}

	@Override
	public int getPlayerScore() {
		return this.playerScore;
	}

	@Override
	public void addReward(int reward) {
		this.playerScore += reward;
	}

	@Override
	public Field getMapStateField(int row, int column) {
		return this.map.getMapState()[column][row];
	}

	@Override
	public boolean isGameOver() {
		return this.isGameOver;
	}

	@Override
	public boolean isHeroKilled() {
		return this.isHeroKilled;
	}

	@Override
	public void setGameOver(boolean gameOver) {
		this.isGameOver = gameOver;
	}

	@Override
	public void setHeroKilled(boolean killed) {
		this.isHeroKilled = killed;
	}

	@Override
	public void setGameStarted(boolean gameStarted) {
		this.isGameStarted = gameStarted;
	}

	@Override
	public boolean isGameStarted() {
		return this.isGameStarted;
	}

	@Override
	public boolean isGamePrepared() {
		return this.isGamePrepared;
	}

	@Override
	public void setGamePrepared(boolean prepared) {
		this.isGamePrepared = prepared;
	}

	@Override
	public void setGameType(GameType gameType) {
		this.gameType = gameType;
	}

	@Override
	public GameType getGameType() {
		return this.gameType;
	}

	// ========== <Serialization> ==========

	@Override
	public GameState getDeepCopy() {
		try {
			// Serialization of object
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			ObjectOutputStream out = new ObjectOutputStream(bos);
			out.writeObject(this);

			// De-serialization of object
			ByteArrayInputStream bis = new ByteArrayInputStream(
					bos.toByteArray());
			ObjectInputStream in = new ObjectInputStream(bis);

			GameState copied = (GameState) in.readObject();

			return copied;
		} catch (ClassNotFoundException | IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	// ========== </Serialization> ==========

	// ========== </Getters / Setters> ==========

	// ========== <Constructors> ==========

	public GameStateImpl(Hero hero1, Hero hero2, Enemy enemy1, Enemy enemy2,
			Enemy enemy3, Enemy enemy4, Map map) {

		this.isHeroKilled = false;
		this.isGameOver = false;
		this.isGameStarted = false;
		this.isGamePrepared = false;

		this.hero1 = hero1;
		this.hero2 = hero2;

		this.enemies = new HashSet<Enemy>();
		this.enemies.add(enemy1);
		this.enemies.add(enemy2);
		this.enemies.add(enemy3);
		this.enemies.add(enemy4);
		this.map = map;

		// Save starting properties of characters
		int charactersCount;
		if (hero2 == null) {
			charactersCount = SINGLEPLAYER_CHARACTERS_COUNT;
		} else {
			charactersCount = MULTIPLAYER_CHARACTERS_COUNT;
		}

		this.charactersStartingProperties = new Object[charactersCount][4];
		// Save characters starting positions
		Character[] characters = new Character[charactersCount];
		characters[0] = enemy1;
		characters[1] = enemy2;
		characters[2] = enemy3;
		characters[3] = enemy4;
		characters[4] = hero1;
		if (hero2 != null) {
			characters[5] = hero2;
		}
		for (int charIdx = 0; charIdx < characters.length; charIdx++) {
			charactersStartingProperties[charIdx][0] = characters[charIdx];
			charactersStartingProperties[charIdx][1] = new Float(
					characters[charIdx].getLocX());
			charactersStartingProperties[charIdx][2] = new Float(
					characters[charIdx].getLocY());
			charactersStartingProperties[charIdx][3] = characters[charIdx]
					.getDirection().toString();
		}

		// // Save starting state of map
		// this.savedMap = ;
	}

	// ========== </Constructors> ==========

	// ========== <Members> ==========

	// Characters
	private Hero hero1;
	private Hero hero2;
	private Set<Enemy> enemies;

	// Other fields
	private Map map;
	private int playerScore;
	private boolean isGameOver;
	private boolean isHeroKilled;
	private boolean isGameStarted;
	private boolean isGamePrepared;
	private GameType gameType;

	private Object[][] charactersStartingProperties;
	// ========== </Members> ==========

	// ========== <Contants> ==========
	public static final int SINGLEPLAYER_CHARACTERS_COUNT = 5;
	public static final int MULTIPLAYER_CHARACTERS_COUNT = 6;
	// ========== </Contants> ==========
}
