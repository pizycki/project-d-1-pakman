package pakman.gamestate.impl;

import pakman.contracts.characters.Enemy;
import pakman.contracts.characters.Hero;
import pakman.contracts.gamestate.GameState;
import pakman.contracts.gamestate.GameStateFactory;
import pakman.contracts.labyrinth.Map;

/**
 * 
 * @author Pawe� I�ycki
 * 
 */
public class GameStateFactoryImpl implements GameStateFactory {

	/**
	 * 
	 * @param hero1
	 * @param hero2
	 * @param e1
	 * @param e2
	 * @param e3
	 * @param e4
	 * @param map
	 * @return
	 */
	public GameState createNewSession(Hero hero1, Hero hero2, Enemy e1,
			Enemy e2, Enemy e3, Enemy e4, Map map) {
		return new GameStateImpl(hero1, hero2, e1, e2, e3, e4, map);
	}
}
